import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/mainviews/AssetList.dart';
import 'package:save_flutter/mainviews/Billvoucher.dart';
import 'package:save_flutter/mainviews/BudgetScreen.dart';
import 'package:save_flutter/mainviews/Cashbankstatement.dart';
import 'package:save_flutter/mainviews/InsuranceList.dart';
import 'package:save_flutter/mainviews/Investmentlist.dart';
import 'package:save_flutter/mainviews/Liability.dart';
import 'package:save_flutter/mainviews/MyDiary.dart';
import 'package:save_flutter/mainviews/Payment.dart';
import 'package:save_flutter/mainviews/Recipt.dart';
import 'package:save_flutter/mainviews/TaskListPage.dart';

import 'mainviews/Wallet.dart';

class HomePage extends StatefulWidget {
  final String title;

  const HomePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyHomePage();
}

class _MyHomePage extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: GridView.count(
      primary: false,
      padding: const EdgeInsets.all(3),
      crossAxisSpacing: 5,
      mainAxisSpacing: 5,
      crossAxisCount: 3,
      children: <Widget>[
        Card(
          elevation: 3,
          child: new InkWell(
            onTap: () {


              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => PaymentPage(title: "Payment",)));

            },
            child: Column(
              children: [
                Expanded(
                  child: Center(
                      child: Image.asset(
                    'images/card.png',
                    width: 40,
                    height: 40,
                    fit: BoxFit.cover,
                  )),
                  flex: 3,
                ),
                Expanded(
                  child: Container(
                    color: Color(0xFF096c6c),
                    child: Center(
                        child: TextButton(
                      onPressed: () {
                        Navigator.push(
                            context, MaterialPageRoute(builder: (_) => PaymentPage(title: "Payment",)));


                      },
                      child: Text('Payments',
                          style: TextStyle(color: Colors.white, fontSize: 12)),
                    )),
                  ),
                  flex: 2,
                )
              ],
            ),
          ),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ReciptPage(title: "Recipt",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/recipt.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => ReciptPage(title: "Recipt",)));



                        },
                        child: Text('Recipts',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => BillPage(title: "Recipt",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/invoice.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {


                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => BillPage(title: "Recipt",)));


                        },
                        child: Text('Billings',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => WalletPage(title: "wallet",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/wallet.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => WalletPage(title: "wallet",)));


                        },
                        child: Text('Wallet',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>
                    CashbankstatementPage(title: "Cashbankstatement")));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/dollar.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) =>
                          CashbankstatementPage(title: "Cashbankstatement")));



                        },
                        child: Text('Cash/Bank statement',
                            maxLines: 2,
                            textAlign: TextAlign.center,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 11,
                              fontWeight: FontWeight.w700,
                            )),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => TaskListPage(title: "notificationpage",)));

              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/tasklist.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => TaskListPage(title: "notificationpage",)));



                        },
                        child: Text('Task',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => AssetListPage(title: "assetlist",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/assets.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => AssetListPage(title: "assetlist",)));


                        },
                        child: Text('Asset',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => LiabilityPage(title: "wallet",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/clipboard.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {


                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => LiabilityPage(title: "wallet",)));



                        },
                        child: Text('Liability',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => InsuranceListPage(title: "Insurance",)));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/lic.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {
                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => InsuranceListPage(title: "Insurance",)));



                        },
                        child: Text('Insurance',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => InvestmentListPage(title: "investmentlist",)));

              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/profits.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => InvestmentListPage(title: "investmentlist",)));


                        },
                        child: Text('Investment',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
          elevation: 3,
          child: new InkWell(
              onTap: () {
                print("tapped");

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>
                    MyDiarypage(title: "My diary")));

              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/journal.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) =>
                              MyDiarypage(title: "My diary")));



                        },
                        child: Text('My diary',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              )),
        ),
        Card(
            elevation: 3,
            child: new InkWell(
              onTap: () {
                print("tapped");
                Navigator.push(
                    context, MaterialPageRoute(builder: (_) =>
                    Budgetpage(title: "Cashbankstatement")));
              },
              child: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Image.asset(
                      'images/budget.png',
                      width: 40,
                      height: 40,
                      fit: BoxFit.cover,
                    )),
                    flex: 3,
                  ),
                  Expanded(
                    child: Container(
                      color: Color(0xFF096c6c),
                      child: Center(
                          child: TextButton(
                        onPressed: () {


                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) =>
                              Budgetpage(title: "Cashbankstatement")));



                        },
                        child: Text('Budget',
                            style:
                                TextStyle(color: Colors.white, fontSize: 12)),
                      )),
                    ),
                    flex: 2,
                  )
                ],
              ),
            ))
      ],
    ));
  }
}
