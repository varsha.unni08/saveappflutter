// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Message.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Message _$MessageFromJson(Map<String, dynamic> json) {
  return Message(
    json['id'] as String,
    json['createdby_id'] as String,
    json['title'] as String,
    json['message'] as String,
    json['created_date'] as String,
    json['message_validity'] as String,
    json['image'] as String,
    json['verified_status'] as String,
    json['user_type'] as String,
    json['state_id'] as String,
    json['free_byuser'] as String,
    json['active_byuser'] as Object,
    json['reg_code'] as Object,
    json['full_name'] as Object,
  );
}

Map<String, dynamic> _$MessageToJson(Message instance) => <String, dynamic>{
      'id': instance.id,
      'createdby_id': instance.createdby_id,
      'title': instance.title,
      'message': instance.message,
      'created_date': instance.created_date,
      'message_validity': instance.message_validity,
      'image': instance.image,
      'verified_status': instance.verified_status,
      'user_type': instance.user_type,
      'state_id': instance.state_id,
      'free_byuser': instance.free_byuser,
      'active_byuser': instance.active_byuser,
      'reg_code': instance.reg_code,
      'full_name': instance.full_name,
    };
