// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'StateData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StateData _$StateDataFromJson(Map<String, dynamic> json) {
  return StateData(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => Statelist.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$StateDataToJson(StateData instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
