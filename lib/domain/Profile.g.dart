// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profile _$ProfileFromJson(Map<String, dynamic> json) {
  return Profile(
    json['id'] as String,
    json['full_name'] as String,
    json['regCode'] as String,
    json['country_id'] as String,
    json['state_id'] as String,
    json['mobile'] as String,
    json['email_id'] as String,
    json['currency'] as String,
    json['join_date'] as String,
    json['activation_date'] as String,
    json['activation_key'] as String,
    json['defaultLang'] as String,
    json['profile_image'] as String,
    json['gdrive_fileid'] as String,
    json['join_source'] as String,
    json['sp_reg_id'] as String,
  );
}

Map<String, dynamic> _$ProfileToJson(Profile instance) => <String, dynamic>{
      'id': instance.id,
      'full_name': instance.full_name,
      'regCode': instance.regCode,
      'country_id': instance.country_id,
      'state_id': instance.state_id,
      'mobile': instance.mobile,
      'email_id': instance.email_id,
      'currency': instance.currency,
      'join_date': instance.join_date,
      'activation_date': instance.activation_date,
      'activation_key': instance.activation_key,
      'defaultLang': instance.defaultLang,
      'profile_image': instance.profile_image,
      'gdrive_fileid': instance.gdrive_fileid,
      'join_source': instance.join_source,
      'sp_reg_id': instance.sp_reg_id,
    };
