// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Statelist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Statelist _$StatelistFromJson(Map<String, dynamic> json) {
  return Statelist(
    json['id'] as String,
    json['code'] as String,
    json['state_name'] as String,
    json['state_code'] as String,
  );
}

Map<String, dynamic> _$StatelistToJson(Statelist instance) => <String, dynamic>{
      'id': instance.id,
      'code': instance.code,
      'state_name': instance.state_name,
      'state_code': instance.state_code,
    };
