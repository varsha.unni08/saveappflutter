// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Profiledata.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Profiledata _$ProfiledataFromJson(Map<String, dynamic> json) {
  return Profiledata(
    json['status'] as int,
    json['message'] as String,
    Profile.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$ProfiledataToJson(Profiledata instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.toJson(),
    };
