// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkSlider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NetworkSlider _$NetworkSliderFromJson(Map<String, dynamic> json) {
  return NetworkSlider(
    json['id'] as String,
    json['imagepath'] as String,
    json['status'] as String,
  );
}

Map<String, dynamic> _$NetworkSliderToJson(NetworkSlider instance) =>
    <String, dynamic>{
      'id': instance.id,
      'imagepath': instance.imagepath,
      'status': instance.status,
    };
