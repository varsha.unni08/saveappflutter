// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkDashboard.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NetworkDashboard _$NetworkDashboardFromJson(Map<String, dynamic> json) {
  return NetworkDashboard(
    json['status'] as int,
    json['message'] as String,
    NetWorkDashboardData.fromJson(json['data'] as Map<String, dynamic>),
  );
}

Map<String, dynamic> _$NetworkDashboardToJson(NetworkDashboard instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.toJson(),
    };
