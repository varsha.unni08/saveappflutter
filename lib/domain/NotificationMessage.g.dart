// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NotificationMessage.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationMessage _$NotificationMessageFromJson(Map<String, dynamic> json) {
  return NotificationMessage(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => MessageData.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NotificationMessageToJson(
        NotificationMessage instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
