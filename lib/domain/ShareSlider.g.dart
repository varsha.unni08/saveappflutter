// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ShareSlider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShareSlider _$ShareSliderFromJson(Map<String, dynamic> json) {
  return ShareSlider(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => ShareSliderData.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$ShareSliderToJson(ShareSlider instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
