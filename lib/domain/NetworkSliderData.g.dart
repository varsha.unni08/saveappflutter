// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkSliderData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NetworkSliderData _$NetworkSliderDataFromJson(Map<String, dynamic> json) {
  return NetworkSliderData(
    json['status'] as int,
    json['message'] as String,
    (json['data'] as List<dynamic>)
        .map((e) => NetworkSlider.fromJson(e as Map<String, dynamic>))
        .toList(),
  );
}

Map<String, dynamic> _$NetworkSliderDataToJson(NetworkSliderData instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
