// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SettingsData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingsData _$SettingsDataFromJson(Map<String, dynamic> json) {
  return SettingsData(
    json['id'] as String,
    json['bv'] as String,
    json['referal_commission'] as String,
    json['one_bv_required_amount'] as String,
    json['renewal_charge'] as String,
    json['one_bv_equalant_value'] as String,
    json['daily_ceiling'] as String,
    json['Percent_tds'] as String,
    json['Percent_tds_nonpan'] as String,
    json['percent_admin_charges'] as String,
    json['renewal_notification_days'] as String,
    json['bill_prefix'] as String,
    json['cgst'] as String,
    json['sgst'] as String,
    json['igst'] as String,
    json['other_tax'] as String,
    json['buffer_count'] as String,
    json['link_image'] as String,
    json['network_image'] as String,
  );
}

Map<String, dynamic> _$SettingsDataToJson(SettingsData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'bv': instance.bv,
      'referal_commission': instance.referal_commission,
      'one_bv_required_amount': instance.one_bv_required_amount,
      'renewal_charge': instance.renewal_charge,
      'one_bv_equalant_value': instance.one_bv_equalant_value,
      'daily_ceiling': instance.daily_ceiling,
      'Percent_tds': instance.Percent_tds,
      'Percent_tds_nonpan': instance.Percent_tds_nonpan,
      'percent_admin_charges': instance.percent_admin_charges,
      'renewal_notification_days': instance.renewal_notification_days,
      'bill_prefix': instance.bill_prefix,
      'cgst': instance.cgst,
      'sgst': instance.sgst,
      'igst': instance.igst,
      'other_tax': instance.other_tax,
      'buffer_count': instance.buffer_count,
      'link_image': instance.link_image,
      'network_image': instance.network_image,
    };
