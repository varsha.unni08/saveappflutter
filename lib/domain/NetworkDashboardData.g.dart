// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'NetworkDashboardData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NetWorkDashboardData _$NetWorkDashboardDataFromJson(Map<String, dynamic> json) {
  return NetWorkDashboardData(
    json['id'] as String,
    json['position'] as String,
    json['position_downline_setup_default'] as String,
    json['position_next'] as String,
    json['binary_left'] as String,
    json['binary_right'] as String,
    json['binary_matched'] as String,
    json['carry_left'] as String,
    json['member_status'] as String,
    json['carry_right'] as String,
    json['binary_amt'] as String,
    json['referral_commission_amt'] as String,
    json['level_amt'] as String,
    json['achievement_amt'] as String,
  );
}

Map<String, dynamic> _$NetWorkDashboardDataToJson(
        NetWorkDashboardData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'position': instance.position,
      'position_downline_setup_default':
          instance.position_downline_setup_default,
      'position_next': instance.position_next,
      'binary_left': instance.binary_left,
      'binary_right': instance.binary_right,
      'binary_matched': instance.binary_matched,
      'carry_left': instance.carry_left,
      'member_status': instance.member_status,
      'carry_right': instance.carry_right,
      'binary_amt': instance.binary_amt,
      'referral_commission_amt': instance.referral_commission_amt,
      'level_amt': instance.level_amt,
      'achievement_amt': instance.achievement_amt,
    };
