// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Head.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Head _$HeadFromJson(Map<String, dynamic> json) {
  return Head(
    json['responseTimestamp'] as String,
    json['version'] as String,
    json['signature'] as String,
  );
}

Map<String, dynamic> _$HeadToJson(Head instance) => <String, dynamic>{
      'responseTimestamp': instance.responseTimestamp,
      'version': instance.version,
      'signature': instance.signature,
    };
