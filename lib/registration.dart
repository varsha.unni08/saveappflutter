import 'dart:convert';

import 'dart:math';

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:save_flutter/domain/CountryData.dart';
import 'package:save_flutter/domain/CountryList.dart';
import 'package:save_flutter/domain/StateData.dart';
import 'package:save_flutter/domain/Statelist.dart';
import 'package:save_flutter/domain/TemregData.dart';
import 'package:save_flutter/domain/country.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:io';
import 'connection/DataConnection.dart';
import 'home.dart';
import 'login.dart';
import 'otp.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:lite_rolling_switch/lite_rolling_switch.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: Registrationpage(title: 'registration'),
      debugShowCheckedModeBanner: false,
    );
  }
}


class Registrationpage extends StatefulWidget{


  Registrationpage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _MyRegPageState createState() => _MyRegPageState();
}



class _MyRegPageState extends State<Registrationpage> {
  int _counter = 0;

 bool  isstateVisible=false,isSponserVisible=false;

  List<CountryData> countrydatalist=[];

  List<Statelist> statedatalist=[];

  List<String> statedata_data=[];

  List<String> countr_data=[];

  late CountryData cddata;

  bool istermsconditionchecked=false;

  List<Countrydata> users = <Countrydata>[
    const Countrydata('Android'),
    const Countrydata('Flutter'),
    const Countrydata('ReactNative'),
    const Countrydata('iOS')
  ];
  String dropdownValue = 'Select country';
  String dropdownstate="Select state";


  String languagedropdown='Select your language';
  String sponsermobilenumber="",sponserregCode="",spregid="";

  String countrid="0",stateid="0";





  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
  }

  @override
  void initState() {
    // TODO: implement initState

    DataConnectionStatus.check().then((value)
    {

      if (value != null && value) {
        // Internet Present Case

        getCountryData();


      }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Check your internet connection"),
        ));

      }
    }
    );


    super.initState();

    // sleep(const Duration(seconds:6));
    // startTime();
  }



  // route() {
  //   Navigator.pushReplacement(context, MaterialPageRoute(
  //       builder: (context) => _MyLoginPageState(title: "dashboard",)
  //   )
  //   );
  // }



  @override
  Widget build(BuildContext context) {


   // TemRegData data=new TemRegData();

    TextEditingController namecontroller=new TextEditingController();

    if(TemRegData.name.isNotEmpty) {
      namecontroller.text = TemRegData.name;
    }
    TextEditingController emailcontroller=new TextEditingController();

    if(TemRegData.email.isNotEmpty) {
      emailcontroller.text = TemRegData.email;
    }
    TextEditingController mobilecontroller=new TextEditingController();

    if(TemRegData.mobilenumber.isNotEmpty) {
      mobilecontroller.text = TemRegData.mobilenumber;
    }

    TextEditingController sponsermobilecontroller=new TextEditingController();

    if(TemRegData.sponsermobilenumber.isNotEmpty) {
      sponsermobilecontroller.text = TemRegData.sponsermobilenumber;
    }

    TextEditingController passwordcontroller=new TextEditingController();

    if(TemRegData.password.isNotEmpty) {
      passwordcontroller.text = TemRegData.password;
    }
    TextEditingController confirmpasswordcontroller=new TextEditingController();
    if(TemRegData.password.isNotEmpty) {
      confirmpasswordcontroller.text = TemRegData.confirmpassword;
    }



    if(TemRegData.sponserregCode.isNotEmpty)
      {
        sponserregCode=TemRegData.sponserregCode;
      }

    if(TemRegData.spregid.isNotEmpty)
    {
      spregid=TemRegData.spregid;
    }


 //getCountryData();

    return new Scaffold(

      appBar: AppBar(
          backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Registration"),
        centerTitle: false,

      ),

      body: Stack(
        children: <Widget>[
          new Container(
            decoration: new BoxDecoration(image: new DecorationImage(image: new AssetImage("images/splashbg.png"), fit: BoxFit.fill)),
          ),
          SingleChildScrollView(
            child: Column(

    children: <Widget>[



    Padding(
    padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
    // padding: EdgeInsets.all(15),
    child: new Theme(data: new ThemeData(
    hintColor: Colors.white,

    ), child: TextField(

      controller: namecontroller,


    decoration: InputDecoration(
    focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 0.5),
    ),
    enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Colors.white, width: 0.5),
    ),
    hintText: 'Name',



    ),

      onChanged: (text) {
        TemRegData.name=text;

      },


    )),
    ),

      Padding(
        padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.white
        ), child: TextField(
          controller: emailcontroller,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            hintText: 'Email',
          ),

          onChanged: (text) {
            TemRegData.email=text;

          },
        )),
      ),

      Padding(
          padding: const EdgeInsets.all(15),
          child:Container(
            width: double.infinity,
            height: 60.0,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                // red as border color
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: true,
                child: InputDecorator(
                  decoration: const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(

                      value: dropdownValue,
                      items: countr_data
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownValue = newValue!;

                          DataConnectionStatus.check().then((value)
                          {

                            if (value != null && value) {
                              // Internet Present Case

                              getState(dropdownValue);


                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Check your internet connection"),
                              ));

                            }
                          }
                          );



                        });

                      },
                    //  value: dropdownValue,
                      style: Theme.of(context).textTheme.bodyText1,

                    ),
                  ),
                ),
              ),
            ),
          )),


    isstateVisible?  Padding(
          padding: const EdgeInsets.all(15),
          child:Container(
            width: double.infinity,
            height: 60.0,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                // red as border color
              ),
            ),
            child: DropdownButtonHideUnderline(

              child: ButtonTheme(
                alignedDropdown: true,
                child: InputDecorator(
                  decoration: const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(

                      isExpanded: true,
                      value: dropdownstate,
                      items: statedata_data
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          dropdownstate = newValue!;

                          checkState(dropdownstate);


                        });
                      },
                      style: Theme.of(context).textTheme.bodyText1,

                    ),
                  ),
                ),
              ),
            ),
          )):new Container(),

      Padding(
        padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
        // padding: EdgeInsets.all(15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.white
        ), child: TextField(
          controller: mobilecontroller,
          keyboardType: TextInputType.number,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            hintText: 'Mobile number',
          ),
          onChanged: (text) {
            TemRegData.mobilenumber=text;

          },
        )),
      ),

  Padding(
        padding: const EdgeInsets.all(15),

        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
    textDirection: TextDirection.rtl,
    children: <Widget>[
    TextButton(onPressed: () {

      DataConnectionStatus.check().then((value)
      {

        if (value != null && value) {
          // Internet Present Case

          getSponser(sponsermobilecontroller.value.text.trim());


        }
        else{

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Check your internet connection"),
          ));

        }
      }
      );






    }, child: Text("Submit"),

    ),
    Expanded(child: new Theme(data: new ThemeData(
        hintColor: Colors.white
    ), child: TextField(
      controller: sponsermobilecontroller,
      keyboardType: TextInputType.number,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.5),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white, width: 0.5),
        ),
        hintText: 'Enter sponser mobile number',
      ),

      onChanged: (text) {
        TemRegData.sponsermobilenumber=text;

      },
    )))
    ],
    ),
      ),


   isSponserVisible?   Padding(
        padding: const EdgeInsets.all(20),

        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          textDirection: TextDirection.ltr,
          children: <Widget>[
            Text('Mobile' ,style: new TextStyle(color: Colors.white),
            ),
            Expanded(child: new Theme(data: new ThemeData(
                hintColor: Colors.white
            ), child: Text( ' :   '+sponsermobilenumber,style: new TextStyle(color: Colors.white),

            )))
          ],
        ),
      ):new Container(),

      isSponserVisible?    Padding(
        padding: const EdgeInsets.all(20),

        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          textDirection: TextDirection.ltr,
          children: <Widget>[
            Text('Reg Code' ,style: new TextStyle(color: Colors.white),
            ),
            Expanded(child: new Theme(data: new ThemeData(
                hintColor: Colors.white
            ), child: Text( ' :   '+sponserregCode,style: new TextStyle(color: Colors.white),

            )))
          ],
        ),
      ):new Container(),






      Padding(
        padding: const EdgeInsets.all(15),
        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.white
        ), child: TextField(
          controller: passwordcontroller,
          obscureText: true,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            hintText: 'Password',
          ),
          onChanged: (text) {
            TemRegData.password=text;

          },
        )),
      ),

      Padding(
        padding: const EdgeInsets.all(15),
        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: new Theme(data: new ThemeData(
            hintColor: Colors.white
        ), child: TextField(
          controller: confirmpasswordcontroller,
          obscureText: true,
          decoration: InputDecoration(
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 0.5),
            ),
            hintText: 'Confirm password',
          ),
            onChanged: (text) {
              TemRegData.confirmpassword=text;

            }
        )),
      ),







      Padding(
          padding: const EdgeInsets.all(15),
          child:Container(
            width: double.infinity,
            height: 60.0,
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                // red as border color
              ),
            ),
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: true,
                child: InputDecorator(
                  decoration: const InputDecoration(border: OutlineInputBorder()),
                  child: DropdownButtonHideUnderline(
                    child: DropdownButton(

                      value: languagedropdown,
                      items: DataConstants.arrofLanguages
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String? newValue) {
                        setState(() {
                          languagedropdown = newValue!;

                          TemRegData.language=languagedropdown;
                        });
                      },
                      style: Theme.of(context).textTheme.bodyText1,

                    ),
                  ),
                ),
              ),
            ),
          )),





      Padding(
        padding: const EdgeInsets.all(20),

        //padding: EdgeInsets.symmetric(horizontal: 15),
        child: Row(
          textDirection: TextDirection.ltr,
          children: <Widget>[
    Checkbox(
    // checkColor: Colors.white10,
    // activeColor: Colors.white,
    value: istermsconditionchecked, onChanged: (bool? value) {

      setState(() {
        istermsconditionchecked=value!;


      });

    },
    )
    ,
            Expanded(child: new Theme(data: new ThemeData(
                hintColor: Colors.white
            ), child: Text( 'I agree your terms and conditions ',style: new TextStyle(color: Colors.white),

            )))
          ],
        ),
      ),

              Padding(
              padding: const EdgeInsets.all(15),
   child :   Container(
        height: 50,
        width: 150,
        decoration: BoxDecoration(
            color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
        child: TextButton(
          onPressed: () {
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

            // TemRegData trmg=new TemRegData(namecontroller.value.text,emailcontroller.value.text
            // ,dropdownValue,dropdownstate,mobilecontroller.value.text,sponsermobilecontroller.value.text,passwordcontroller.value.text,languagedropdown);



            checkAllDatas(namecontroller.value.text,emailcontroller.value.text,countrid,stateid,mobilecontroller.value.text,
           spregid,sponserregCode,passwordcontroller.value.text,confirmpasswordcontroller.value.text,languagedropdown,istermsconditionchecked );
          },
          child: Text(
            'Submit',
            style: TextStyle(color: Colors.white, fontSize: 15),
          ),
        ),
      ),),


    Padding(
    padding: const EdgeInsets.all(15),

        child:  TextButton(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        ),
        onPressed: () {

          Navigator.of(context).pop();
        },
        child: Text('Already a member ? Login'),
      ))



    ]






            ),
          ),
        ],
      ),
    );





  }



 getCountryData() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
        Uri.parse(DataConstants.baseurl+DataConstants.getCountry+"?timestamp="+date),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    var jsondata=jsonDecode(response);
    print(jsondata);
    CountryList countryList=CountryList.fromJson(jsondata);
    countrydatalist= countryList.data;
    statedata_data.clear();
    List<String>stdata=[];

    stdata.add("Select country");
    for(CountryData d in countrydatalist)
  {


    stdata.add(d.country_name);





  }
    setState(() {
      countr_data=stdata;
      statedata_data=[];
});

  }
  
  
  getState(String country) async
  {

    String countryid="0";
    for(CountryData d in countrydatalist)
    {
      if(country.compareTo(d.country_name)==0) {
        countryid = d.id;
        countrid=d.id;

        TemRegData.country=country;
        TemRegData.countryid=countrid;

      }

    }

    
    
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.getState+"?countryid="+countryid+"&timestamp="+date),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    var jsondata=jsonDecode(response);
    print(jsondata['status']);

    if(jsondata['status']==1) {
      try {
        StateData statelist = StateData.fromJson(jsondata);

        statedatalist = statelist.data;

        List<String>stdata = [];
        statedata_data = [];
        stdata.add("Select state");
        for (Statelist st in statelist.data) {
          stdata.add(st.state_name);
        }

        setState(() {
          if (countryid.compareTo("1") == 0) {
            isstateVisible = true;
            statedata_data = stdata;
            print(" matched");
          }
          else {
            isstateVisible = false;
            statedata_data = [];

            print("not match");
          }
        });
      } on Exception catch (_) {
        print('never reached');


      }
    }else{

      setState(() {
        isstateVisible = false;
        statedata_data = [];

        print("not match");
      });
    }
    
  }


  checkState(String state)
  {

    for (Statelist sd in statedatalist)
      {

        if(sd.state_name.compareTo(state)==0)
          {

            stateid=sd.id;
            TemRegData.state=state;
            TemRegData.stateid=sd.id;
          }

      }
  }

  getSponser(String mobilenumber) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.checkSponsor+"?mobile="+mobilenumber+"&timestamp="+date),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    var jsondata=jsonDecode(response);
    print(jsondata);

    if(jsondata['status']==1)
      {
        setState(() {
          isSponserVisible=true;

         Map<String,dynamic> jsarray= jsondata['data'];

         print( "Country data : "+jsarray['reg_code']);

         sponserregCode= jsarray['reg_code'];
          sponsermobilenumber= jsarray['mobile'];
          spregid=jsarray['reg_id'];

          TemRegData.sponserregCode=sponserregCode;
          TemRegData.spregid=spregid;




          //sponsermobilenumber=
        });
      }
    else{

      setState(() {
        sponserregCode= "";
        sponsermobilenumber= "";
        spregid="";

        TemRegData.sponserregCode=sponserregCode;
        TemRegData.spregid=spregid;

        isSponserVisible=false;
      });


    }


  }

  checkAllDatas(String name,String email,String countryid,String state,String mobilenumber,String spregid,String regcode,String password,String confirmpassword,String language,bool ischecked){


    if(name.isNotEmpty)
      {
        if(email.isNotEmpty)
        {

          if(DataConstants.checkEmailPattern(email)) {
            if (countryid.compareTo("0") != 0) {
              if (mobilenumber.isNotEmpty) {
                if (sponserregCode.isNotEmpty && spregid.isNotEmpty) {
                  if (password.isNotEmpty) {
                    if (password.compareTo(confirmpassword) == 0) {
                      if (languagedropdown.compareTo("Select your language") !=
                          0) {
                        if (istermsconditionchecked) {

                          DataConnectionStatus.check().then((value)
                          {

                            if (value != null && value) {


                              DataConnectionStatus.check().then((value)
                              {

                                if (value != null && value) {
                                  // Internet Present Case

                                  checkMobileNumber(mobilenumber);


                                }
                                else{

                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    content: Text("Check your internet connection"),
                                  ));

                                }
                              }
                              );
                              // Internet Present Case




                            }
                            else{

                              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                content: Text("Check your internet connection"),
                              ));

                            }
                          }
                          );



                        }
                        else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(
                                "Please check your terms and conditions"),
                          ));
                        }
                      }
                      else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(" Select your language"),
                        ));
                      }
                    }
                    else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(" Password confirmation failed"),
                      ));
                    }
                  }
                  else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Enter password"),
                    ));
                  }
                }
                else {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Select sponser"),
                  ));
                }
              }
              else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Enter mobile number"),
                ));
              }
            }
            else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("Select country"),
              ));
            }
          }
          else{

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Enter a valid email"),
            ));
          }



        }
        else{


          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter the email"),
          ));
        }
      }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Enter the name"),
      ));
    }




    // Navigator.push(
    //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

  }

  checkMobileNumber(String mobilenumber ) async
  {



    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.getUserByMobile+'?mobile='+mobilenumber+'&timestamp='+date),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

      //     body: <String, String>{
      //   'Content-Type': 'application/x-www-form-urlencoded',
      //
      //   'uuid': date,
      //   'timestamp': date
      // }

    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    // print(jsondata);

    int status = jsondata['status'];

    if(status==0)
    {

      Navigator.push(
          context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text("This mobile number does not  exists"),
      // ));

      // var rng = new Random();
      // var code = rng.nextInt(9000) + 1000;
      //
      // print('otp code is : '+code.toString());
      //
      //
      // sendOtpCode(code,mobilenumber);
      // showOtpCodeDialog(code,otpcodecontroller,mobilenumber);
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("This mobile number  exist"),
      ));
    }




  }


}