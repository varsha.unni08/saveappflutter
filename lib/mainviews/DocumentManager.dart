import 'dart:convert';
import 'dart:io';
import 'package:http/io_client.dart';
import 'package:http/http.dart' as http;


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';
import 'package:save_flutter/domain/DriveFiles.dart';
import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:googleapis/drive/v3.dart' as drive;
import 'package:file_picker/file_picker.dart';

import 'package:share/share.dart';



import 'package:save_flutter/domain/Paymentdata.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:save_flutter/mainviews/AddRecipt.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;


class DocumentManagerPage extends StatefulWidget {
  final String title;

  const DocumentManagerPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _DocumentManagerPage();

}


class _DocumentManagerPage extends State<DocumentManagerPage> {


  String date = "";
  String date1 = "";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Account name", "Debit", "Credit", "Action"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<Cashbankaccount> cashbankdata = [];

  String selecteddata="";

  String filepath="";

  TextEditingController titleController=new TextEditingController();

  List<DriveFile>drfile=[];

  @override
  void initState() {
    // TODO: implement initState

    getFileData();


    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Document manager",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: Stack(
      children: [


        (drfile.length>0) ?  Padding(padding: EdgeInsets.fromLTRB(0, 10, 0, 0),




            child: MediaQuery.removePadding(
              context: context,
              removeTop: true, child:ListView.builder(
              physics: BouncingScrollPhysics(),

              shrinkWrap: true,

              itemCount: drfile.length,
              itemBuilder: (context, index) {
                return  Container(
                  width: double.infinity,
                    height: 150,
                    child: Card(elevation: 3,

                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.all(6),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text("Title  "),
                                  flex: 2,
                                ),
                                Expanded(
                                  child: Text(":"),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Text(drfile[index].title
                                   ,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  flex: 2,
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(6),
                            child: Row(
                              children: [
                                Expanded(
                                  child: Text("File name"),
                                  flex: 2,
                                ),
                                Expanded(
                                  child: Text(":"),
                                  flex: 1,
                                ),
                                Expanded(
                                  child: Text(
                                    drfile[index].filename,
                                    style: TextStyle(fontSize: 13),
                                  ),
                                  flex: 2,
                                )
                              ],
                            ),
                          ),

                          Divider(
                            thickness: 1,
                            color: Colors.black26,
                          ),
                          Padding(
                              padding: EdgeInsets.all(2),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: TextButton(
                                      onPressed: () async {



                                        deleteFileData( drfile[index].fileid, drfile[index].filename,drfile[index].id);






                                      },
                                      child: Text(
                                        "Delete",
                                        style: TextStyle(
                                            color: Colors.redAccent),
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                  Container(
                                    width: 1,
                                    height: 40,
                                    color: Colors.grey,
                                  ),
                                  Expanded(
                                    child: TextButton(
                                      onPressed: () {



                                        downloadFileData( drfile[index].fileid, drfile[index].filename);


                                      },
                                      child: Text(
                                        "Download",
                                        style: TextStyle(
                                            color: Colors.green),
                                      ),
                                    ),
                                    flex: 2,
                                  ),
                                ],
                              ))
                        ],
                      ),




                    ),);


              },
            )
              ,)



        ):Container(),









        Align(alignment: Alignment.bottomRight,

            child:Padding(
                padding: EdgeInsets.all(12),

                child: FloatingActionButton(
                  onPressed: () async{


                    Dialog forgotpasswordDialog = Dialog(
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)), //this right here
                      child: Container(
                        height: 300.0,
                        width: 700.0,

                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding:  EdgeInsets.all(15.0),
                              child: new Theme(data: new ThemeData(
                                  hintColor: Colors.black38
                              ), child: TextField(

                                keyboardType: TextInputType.text,
                                controller: titleController,

                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                  ),
                                  enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                                  ),
                                  hintText: 'Enter the title',


                                ),
                              )),
                            ),

                            Padding(padding: EdgeInsets.all(8),

                            child:TextButton(onPressed: () {



                              showDialog(
                                  context: context,
                                  builder: (_) {
                                    return MyFileOptionsDialog();
                                  }).then((value) => {

                                selecteddata =value["option"].toString(),

                                if(selecteddata.compareTo("1")==0)
                                  {

                                    filePickThroughGallery().then((value) => {




                                      filepath=value.toString()



                                    })
                                  }
                                else if(selecteddata.compareTo("2")==0)
                                  {
                                    filePickThroughdocument().then((value) => {

                                      filepath=value.toString()

                                    })
                                  }








                              });


                            }, child: Text("Select a file",style: TextStyle(fontSize: 14),),) ,),



                            Padding(padding: EdgeInsets.all(8),

                              child:Text(filepath) ,),

                            Padding(
                              padding: EdgeInsets.all(15.0),

                              child: Container(

                                width: 150,
                                height: 55,
                                decoration: BoxDecoration(

                                    color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                                child:Align(
                                  alignment: Alignment.center,
                                  child: TextButton(

                                    onPressed:() {

                                      Navigator.pop(context);

                                      if(titleController.text.isNotEmpty) {

                                        if(filepath.isNotEmpty)
                                          {



                                            uploadFile(filepath,titleController.text);
                                            titleController.text="";
                                            filepath="";
                                          }
                                        else{

                                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                            content: Text("Select a file"),
                                          ));
                                        }





                                      }
                                      else{

                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                          content: Text("Enter the title"),
                                        ));
                                      }


                                    },

                                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                                ),



                                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                              ),


                              // ,
                            ),
                            // Padding(padding: EdgeInsets.only(top: 50.0)),
                            // TextButton(onPressed: () {
                            //   Navigator.of(context).pop();
                            // },
                            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                          ],
                        ),
                      ),
                    );



                    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);





















                  },
                  child: Icon(Icons.add, color: Colors.white, size: 29,),
                  backgroundColor: Colors.blue,

                  elevation: 5,
                  splashColor: Colors.grey,
                ))),
        ]

      )

      );
  }

  void deleteFileData(String fileid,String filename,String id) async{
    final dbHelper = new DatabaseHelper();
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    var dri = drive.DriveApi(client);
    await dri.files.delete(fileid);


dbHelper.deleteDataByid(id, DatabaseTables.TABLE_DOCUMENT);



    getFileData();




  }


  void downloadFileData(String fileid,String filename) async
  {
    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount =
        await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );
    var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    var dri = drive.DriveApi(client);
    drive.Media file = await dri.files.get( fileid, downloadOptions: drive.DownloadOptions.fullMedia) as drive.Media;

   // var response = await dri.files.delete(fileId)

    final directory = await getExternalStorageDirectory();
    print(directory!.path);
    final saveFile = File('${directory.path}/${new DateTime.now().millisecondsSinceEpoch}$filename');

    List<int> dataStore = [];




    file.stream.listen((data) {
      print("DataReceived: ${data.length}");
      dataStore.insertAll(dataStore.length, data);
    }, onDone: () {
      print("Task Done");
      saveFile.writeAsBytes(dataStore);
      print("File saved at ${saveFile.path}");

      Share.shareFiles([saveFile.path],
          text: "File from google drive");





    }, onError: (error) {
      print("Some Error");
    });





  }



  void getFileData() async
  {
    final dbHelper = new DatabaseHelper();

    List<Map<String, dynamic>> v =
    await dbHelper.queryAllRows(DatabaseTables.TABLE_DOCUMENT);

    List<DriveFile>drf=[];

    for(Map a in v) {

      int id = a["keyid"];
      String data = a["data"];

      var jsondata = jsonDecode(data);


      String name = jsondata['name'];
      String fileid=jsondata['fileid'];
      String filename=jsondata['filename'];

      DriveFile filed=new DriveFile();
      filed.title=name;
      filed.fileid=fileid;
      filed.filename=filename;
      filed.id=id.toString();
      drf.add(filed);




    }


setState(() {

  drfile=drf;

});




  }

  void uploadFile(String path,String title)async
  {

    print(path);

    final dbHelper = new DatabaseHelper();


    File file=new File(path);

    final GoogleSignIn googleSignIn = GoogleSignIn();

    final GoogleSignInAccount googleSignInAccount =
    await googleSignIn.signIn();

    final GoogleSignInAuthentication googleSignInAuthentication =
    await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.credential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );



    var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    var dri = drive.DriveApi(client);
    drive.File fileToUpload = drive.File();

    fileToUpload.parents = ["root"];

    var response = await dri.files.create(
      fileToUpload,
      uploadMedia: drive.Media(file!.openRead(), file!.lengthSync()),
    );


    print(response.id);


    var mjobject=new Map();
    mjobject['name']=title;
    mjobject['fileid']=response.id;
    mjobject['filename']=file.path.split('/').last;

    var js=json.encode(mjobject);

    Map<String, dynamic> data_To_Table=new Map();
    data_To_Table['data']=js.toString();
    int id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_DOCUMENT);


    getFileData();



  }
}















Future<String> filePickThroughdocument()async
{
  FilePickerResult? result = await FilePicker.platform.pickFiles();

  File? file=null ;

  if (result != null) {
     file = File(result.files.single.path!);

    // print(file.path);
    //
    //




  } else {
    // User canceled the picker
  }


  return file!.path;
}




Future<String> filePickThroughGallery() async
{
  final ImagePicker _picker = ImagePicker();
  // Pick an image
  final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
  File? croppedFile =null;
  // print(image!.path) ;

  if(image!=null) {
    croppedFile = await ImageCropper
        .cropImage(
        sourcePath: image.path,
        aspectRatioPresets: [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset
                .original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
    );
  }

    return croppedFile!.path;

    //  uploadImage(croppedFile);

    // final GoogleSignIn googleSignIn = GoogleSignIn();
    //
    // final GoogleSignInAccount googleSignInAccount =
    //     await googleSignIn.signIn();
    //
    // // final GoogleSignInAuthentication googleSignInAuthentication =
    // // await googleSignInAccount.authentication;
    // //
    // // final AuthCredential credential = GoogleAuthProvider.credential(
    // //   accessToken: googleSignInAuthentication.accessToken,
    // //   idToken: googleSignInAuthentication.idToken,
    // // );
    //
    //
    //
    // var client = GoogleHttpClient(await googleSignInAccount.authHeaders);
    // var dri = drive.DriveApi(client);
    // drive.File fileToUpload = drive.File();
    //
    // fileToUpload.parents = ["root"];
    //
    // var response = await dri.files.create(
    //   fileToUpload,
    //   uploadMedia: drive.Media(croppedFile!.openRead(), croppedFile!.lengthSync()),
    // );
    //
    //
    // print(response.id);




}











class GoogleHttpClient extends http.BaseClient {
  Map<String, String> _headers;
  GoogleHttpClient(this._headers) : super();



  final http.Client _client = new http.Client();



  Future<http.StreamedResponse> send(http.BaseRequest request) {
    return _client.send(request..headers.addAll(_headers));
  }


}



class MyFileOptionsDialog extends StatefulWidget {





  @override
  _MyDialogState createState() => new _MyDialogState();


}


class _MyDialogState extends State<MyFileOptionsDialog> {



  int galleryOrFile=0;

  int gallery=1;
  int file=2;





  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AlertDialog(
        content: Container(
        height: 200.0,
        width: 400.0,

          child: Row(

            children: [

              Expanded(child: Column(

                children: [

                  GestureDetector(

                    onTap: (){
                      galleryOrFile=file;
                      Navigator.pop(context,{'option':galleryOrFile});


                    },

                    child:  Image.asset('images/folder.png',width: 50,height: 50,fit: BoxFit.fill,),



                  ),


                  GestureDetector(

                    onTap: (){
                      galleryOrFile=file;
                      Navigator.pop(context,{'option':galleryOrFile});

                    },

                    child:   Padding(padding: EdgeInsets.all(5),child: Text("File",style: TextStyle(fontSize: 13,color: Colors.black),),)



                  )








                ],


              ),flex: 1,),

              Expanded(child: Column(

                children: [


                  GestureDetector(


                    onTap: (){

                      galleryOrFile=gallery;

                      Navigator.pop(context,{'option':galleryOrFile});
                    },

                    child:  Image.asset('images/gallery.png',width: 50,height: 50,fit: BoxFit.fill,),
                  ),

                  GestureDetector(

                    onTap: (){

                      galleryOrFile=gallery;


                      Navigator.pop(context,{'option':galleryOrFile});



                    },

                    child:  Padding(padding: EdgeInsets.all(5),child: Text("Gallery",style: TextStyle(fontSize: 13,color: Colors.black),),) ,




                  )






                ],


              ),flex: 1,)





            ],


          ),

    )

    );
  }





}