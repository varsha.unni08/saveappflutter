import 'dart:convert' show Utf8Encoder, base64, json, jsonDecode, utf8;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/Accountsettings.dart';
import 'package:save_flutter/domain/Passworddata.dart';
import 'package:save_flutter/mainviews/AddAccountSetup.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Accountsettings',
      theme: ThemeData(primarySwatch: Colors.blueGrey),
      home:
      PasswordManagerpage(title: 'Accountsettings'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class PasswordManagerpage extends StatefulWidget {
  final String title;



  const PasswordManagerpage(
      {Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _PasswordManagerpage();
}

class _PasswordManagerpage extends State<PasswordManagerpage> {


  final dbHelper = new DatabaseHelper();


  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];
  List<Map<String, dynamic>> accountdata = [];

  int bid = 2,
      cid = 0;

  int selecteddataid=0;

  List<AccountSetupData> accsetupdata = [];

  List<AccountSetupData> accsetupdatadummy = [];
  final List<String> entries = <String>['A', 'B', 'C'];

  List<SavedPasswords>savedpassworddata=[];

  @override
  void initState() {
    // TODO: implement initState
    showPasswordDetails();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(


        appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
    leading: IconButton(
    icon: Icon(Icons.arrow_back, color: Colors.white),
    onPressed: () => Navigator.of(context).pop(),
    ),
    title: Text("Password manager"),
    centerTitle: false,
    ),
    body: Container(
    width: double.infinity,
    height: double.infinity,

    child: Stack(
    children: [
      Align(
          alignment: Alignment.topLeft,
          child: Padding(
            padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
            child: ListView.builder(
                padding: const EdgeInsets.all(8),
                itemCount: savedpassworddata.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    // color: Colors.amber[colorCodes[index]],
                    child: Center(
                        child: Card(
                          elevation: 5,
                          child: Container(
                            color: Colors.white,
                            width: double.infinity,
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text("Title",style: TextStyle(fontSize: 13,color: Colors.black)),
                                        flex: 2,
                                      ),
                                      Expanded(
                                        child: Text(":"),
                                        flex: 1,
                                      ),
                                      Expanded(
                                        child: Text(
                                          savedpassworddata[index].title,
                                          style: TextStyle(fontSize: 15,color: Colors.black),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: Text("Username",style: TextStyle(fontSize: 13,color: Colors.black)),
                                        flex: 2,
                                      ),
                                      Expanded(
                                        child: Text(":"),
                                        flex: 1,
                                      ),
                                      Expanded(
                                        child: Text(
                                          savedpassworddata[index].username,
                                          style: TextStyle(fontSize: 15,color: Colors.black),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),


                                Padding(
                                    padding: EdgeInsets.all(2),
                                    child: Stack(
                                      children: [
                                        Align(


                                          alignment: Alignment.bottomRight,
                                          child: Padding(

                                            padding: EdgeInsets.fromLTRB(0, 0, 90, 0),
                                            child:TextButton(
                                            onPressed: () {

                                             // deleteConfirmation(index, savedpassworddata[index].id);

                                              selecteddataid=int.parse(savedpassworddata[index].id);

                                              showDialog(
                                                  context: context,
                                                  builder: (_) {
                                                    return MyDialog(selecteddataid);
                                                  }).then((value) => {


                                                if(value['Passwordadded']==1)
                                                  {

                                                    showPasswordDetails()

                                                  }




                                              });

                                            },
                                            child: Text(
                                              "Edit",
                                              style: TextStyle(
                                                  color: Colors.green),
                                            ),
                                          ),
                                        ),),

                                   Align(


                                     alignment: Alignment.bottomRight,
                                     child: TextButton(
                                            onPressed: () {

                                              deleteConfirmation(index, savedpassworddata[index].id);


                                            },
                                            child: Text(
                                              "Delete",
                                              style: TextStyle(
                                                  color: Colors.redAccent),
                                            ),
                                          ),
                                   )

                                      ],
                                    ))
                              ],
                            ),
                          ),
                        )),
                  );
                }),
          )),

      Align(
          alignment: Alignment.bottomRight,
          child: Padding(
              padding: EdgeInsets.all(12),
              child: FloatingActionButton(
                onPressed: () async {

                  showDialog(
                      context: context,
                      builder: (_) {
                        return MyDialog(0);
                      }).then((value) => {


                        if(value['Passwordadded']==1)
                          {

                            showPasswordDetails()

                          }




                  });



                },
                child: Icon(
                  Icons.add,
                  color: Colors.white,
                  size: 29,
                ),
                backgroundColor: Colors.blue,
                elevation: 5,
                splashColor: Colors.grey,
              ))),

      ]
    )
    )
    );
  }


  deleteConfirmation(int index,String id) async
  {



      Widget okButton = TextButton(
        child: Text("Yes"),
        onPressed: () {
          Navigator.of(context).pop();

          dbHelper.deleteDataByid(id, DatabaseTables.TABLE_PASSWORD).then((value) {


            showPasswordDetails();

          });


        },
      );

      Widget noButton = TextButton(
        child: Text("No"),
        onPressed: () {
          Navigator.of(context).pop();
        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Save"),
        content: Text(
            "Do you want to delete now ?"),
        actions: [
          okButton, noButton
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );

  }

  showPasswordDetails()
  async {

    var data= await dbHelper.queryAllRows(DatabaseTables.TABLE_PASSWORD);

    List<Map<String, dynamic>> m=data;

    List<SavedPasswords>savedpasswords=[];

    for(int i=0;i<m.length;i++)
      {
        Map<String, dynamic> passworddata=m[i];
        String data = passworddata["data"];
        var jsondata = jsonDecode(data);
        String username = jsondata["username"];
        String title = jsondata["title"];
        String keyid=passworddata['keyid'].toString();
        SavedPasswords passwords=new SavedPasswords(keyid, title, username);
        savedpasswords.add(passwords);
      }

    setState(() {

      savedpassworddata.clear();

      savedpassworddata=savedpasswords;
    });






  }










}

class MyDialog extends StatefulWidget {

  int selecteddataid=0;


  MyDialog(this.selecteddataid);

  @override
  _MyDialogState createState() => new _MyDialogState(selecteddataid);
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  final int passwordid;


  _MyDialogState(this.passwordid);

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  TextEditingController titlecontroller=new TextEditingController();
  TextEditingController usernamecontroller=new TextEditingController();
  TextEditingController passwordcontroller=new TextEditingController();

  TextEditingController websitecontroller=new TextEditingController();
  TextEditingController remarkscontroller=new TextEditingController();

  final dbHelper = new DatabaseHelper();

  @override
  void initState()  {
    // TODO: implement initState




    setDataforEdit();


    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        width: MediaQuery.of(context).size.width ,
        height: MediaQuery.of(context).size.height,

        child:Column(

          children: [

            Expanded(

              child: GridView.count(


          crossAxisSpacing: 1,
          mainAxisSpacing: 1,
          crossAxisCount: 1,
          shrinkWrap: true,
          childAspectRatio: 3.4,
          children: <Widget>[

        Padding(
        padding: const EdgeInsets.all(4),
          child: Center(child: Text("Password manager",style: TextStyle(color: Colors.black,fontSize: 20),),),



        ),

            Padding(
              padding: const EdgeInsets.all(4),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: titlecontroller,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Title',
                ),

                onChanged: (text) {


                },
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: usernamecontroller,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Username',
                ),

                onChanged: (text) {


                },
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                obscureText: true,
                controller: passwordcontroller,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Password',
                ),

                onChanged: (text) {


                },
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: websitecontroller,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Website',
                ),

                onChanged: (text) {


                },
              )),
            ),
            Padding(
              padding: const EdgeInsets.all(4),
              // padding: EdgeInsets.all(15),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: remarkscontroller,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Remarks',
                ),

                onChanged: (text) {


                },
              )),
            ),


            ]),









              flex: 4,),
            Expanded(child:  Padding(
              padding: const EdgeInsets.fromLTRB(10, 10, 10, 18),
              child :   Container(
                height:30,
                width: 100,

                child: TextButton(
                  onPressed: () {


                    if(titlecontroller.text.isNotEmpty)
                    {
                      if(usernamecontroller.text.isNotEmpty)
                      {
                        if(passwordcontroller.text.isNotEmpty)
                        {

                          var mjobject=new Map();
                          mjobject['title']=titlecontroller.text;
                          mjobject['username']=usernamecontroller.text;
                          mjobject['password']= passwordcontroller.text;
                          mjobject['website']=websitecontroller.text;
                          mjobject['remarks']=remarkscontroller.text;

                          var js=json.encode(mjobject);

                          Map<String, dynamic> data_To_Table=new Map();
                          data_To_Table['data']=js.toString();

                          if(passwordid!=0) {
                            dbHelper.update(
                                data_To_Table, DatabaseTables.TABLE_PASSWORD,passwordid.toString());
                          }
                          else{
                            dbHelper.insert(
                                data_To_Table, DatabaseTables.TABLE_PASSWORD);
                          }

                          Navigator.of(context).pop({'Passwordadded':1});



                        }
                        else{
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Enter the password"),
                          ));
                        }
                      }
                      else{
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter the username"),
                        ));
                      }
                    }
                    else{
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Enter the title"),
                      ));
                    }





                              },
                  child: Text(
                    'Submit',
                    style: TextStyle(color: Colors.blue, fontSize: 15),
                  ),
                ),
              ),),flex: 1,)


          ],

        )




      ),

    );
  }



  setDataforEdit()async
  {

    var a= await dbHelper.getDataByid(DatabaseTables.TABLE_PASSWORD,passwordid.toString());
    setState(() {

      if(passwordid!=0)
      {



        List<Map<String, dynamic>> m=a;

        Map<String, dynamic> passworddata=m[0];
        String data = passworddata["data"];
        var jsondata = jsonDecode(data);
        String username = jsondata["username"];
        String title = jsondata["title"];




        usernamecontroller.text=username;
        titlecontroller.text=title;
        websitecontroller.text=jsondata['website'];
        remarkscontroller.text=jsondata['remarks'];
        passwordcontroller.text=jsondata['password'];
      }



    });
  }



}