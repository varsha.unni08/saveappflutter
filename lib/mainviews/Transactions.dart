import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';
import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';


import 'package:save_flutter/domain/Paymentdata.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:save_flutter/mainviews/AddRecipt.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;


class TransactionPage extends StatefulWidget {
  final String title;

  const TransactionPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TransactionPage();

}


class _TransactionPage extends State<TransactionPage> {


  String date = "";
  String date1="";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Date", "Amount", "Debit", "Credit"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> ledgerdata = [];

  List<Cashbankaccount> cashbankdata = [];


  @override
  void initState() {
    // TODO: implement initState
   // showCurrentMonthAndYear();

    showDates();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(

      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        automaticallyImplyLeading: false,

        flexibleSpace: Container(

            color: Color(0xFF096c6c),
            width: double.infinity,
            padding: EdgeInsets.all(10),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(2, 50, 0, 0),
                        alignment: Alignment.center,
                        child: Center(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  'images/rightarrow.png',
                                  width: 24,
                                  height: 24,
                                  fit: BoxFit.contain,
                                )))),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(10, 50, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Transactions",
                          style: TextStyle(
                              fontSize: 13, color: Colors.white),
                        )),
                    flex: 5,
                  ),
                ])) ,
        backgroundColor: Color(0xFF096c6c),
        centerTitle: false,

      ),


      body: Stack(
        alignment: Alignment.center,

        children: [

        Align(

        alignment: Alignment.topCenter,
        child: Row(
          children: [

            Expanded(child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            "images/calendar.png",
                            width: 25,
                            height: 25,
                          )),
                      Expanded(
                          child: TextButton(
                            onPressed: () {
                              var now = DateTime.now();

                              date= now.day
                                  .toString() +
                                  "-" +
                                  now.month
                                      .toString() +
                                  "-" +
                                  now.year
                                      .toString();

                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {



                                                date= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                datetxt1=date;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt1 = date;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });



                              // showCustomDatePicker(build(context));


                            },
                            child: Text(
                              datetxt1,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12),
                            ),
                          ))
                    ],
                  ),
                )),flex: 1,),
            Expanded(child: Padding(
                padding: EdgeInsets.all(10),
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.black38)),
                  child: Row(
                    textDirection: ui.TextDirection.rtl,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.all(10),
                          child: Image.asset(
                            "images/calendar.png",
                            width: 25,
                            height: 25,
                          )),
                      Expanded(
                          child: TextButton(
                            onPressed: () {
                              var now = DateTime.now();

                              date1= now.day
                                  .toString() +
                                  "-" +
                                  now.month
                                      .toString() +
                                  "-" +
                                  now.year
                                      .toString();

                              // showCustomDatePicker(build(context));

                              showModalBottomSheet(
                                  context: context,
                                  builder: (context) {
                                    return Container(
                                        child: Column(children: [
                                          Expanded(
                                            child: CupertinoDatePicker(
                                              mode: CupertinoDatePickerMode
                                                  .date,
                                              initialDateTime: DateTime(
                                                  now.year,
                                                  now.month,
                                                  now.day),
                                              onDateTimeChanged:
                                                  (DateTime newDateTime) {


                                                date1= newDateTime.day
                                                    .toString() +
                                                    "-" +
                                                    newDateTime.month
                                                        .toString() +
                                                    "-" +
                                                    newDateTime.year
                                                        .toString();


                                                datetxt2=date1;



                                                //print(date);
                                                // Do something
                                              },
                                            ),
                                            flex: 2,
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.all(2),
                                            child: Container(
                                              height: 50,
                                              width: 150,
                                              decoration: BoxDecoration(
                                                  color: Color(0xF0233048),
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      10)),
                                              child: TextButton(
                                                onPressed: () {
                                                  Navigator.pop(context);

                                                  setState(() {
                                                    datetxt2 = date1;
                                                  });
                                                },
                                                child: Text(
                                                  'Ok',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                          )
                                        ]));
                                  });


                              // showCustomDatePicker(build(context));


                            },
                            child: Text(
                              datetxt2,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12),
                            ),
                          ))
                    ],
                  ),
                )),flex: 1,)


          ],

        ),
      ),


          Padding(padding: EdgeInsets.fromLTRB(0, 70, 0, 0),
            child:      Align(         alignment: Alignment.topCenter,child: Padding(
              padding: const EdgeInsets.all(2),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius: BorderRadius.circular(10)),
                child: TextButton(
                  onPressed: () {

                   showData();

                  },
                  child: Text(
                    'Submit',
                    style:
                    TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ),
              ),
            ),),),


        (ledgerdata.length>0)?Padding(padding: EdgeInsets.fromLTRB(0, 150, 0, 0),

            child:

            Align(alignment: Alignment.topCenter,child: MediaQuery.removePadding(
                context: context,
                removeTop: true,
                child:GridView.builder(
                  physics: BouncingScrollPhysics(),

                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4,
                    crossAxisSpacing: 0.0,
                    mainAxisSpacing: 0.0,
                    childAspectRatio:2
                  ),
                  itemCount: tableheaddata.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding:EdgeInsets.all(0),

                      child: Container(
                        height: 100,
                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black54,
                              width: 0.3,
                            ),
                          ),


                          child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),))),

                    );
                  },
                )),)):Container(),



          Padding(padding: EdgeInsets.fromLTRB(0, 200, 0, 0),
            child: Column(
                children: [
                  Expanded(
                    child: Container(child:
            MediaQuery.removePadding(
                context: context,
                removeTop: true,
                        child: GridView.builder(
                          physics: BouncingScrollPhysics(),

                          shrinkWrap: true,
                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 4,
                            crossAxisSpacing: 0.0,
                            mainAxisSpacing: 0.0,
                            childAspectRatio: 0.7
                          ),
                          itemCount: ledgerdata.length,
                          itemBuilder: (context, index) {
                            return Container(

                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.black54,
                                    width: 0.3,
                                  ),
                                ),




                                child:


                                    Center(child:Text(ledgerdata[index],style: TextStyle(fontSize: 14,color: Colors.black),)));
                          },
                        ))
                    ),flex: 3,
                  ),

                ]
            ),)

        ]

      )




    );
  }

  void showDates()
  {
    List<String> cashbankaccountatanow = [];

    var now=DateTime.now();

    // datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
    // datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    setState(() {

      datetxt1="1"+"-"+now.month.toString()+"-"+now.year.toString();
      datetxt2=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();



    });

    showData();
  }

  void showData() async
  {

    List<String>data=[];

    DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(datetxt1);

    DateTime accountsdateselected = new DateFormat("dd-MM-yyyy").parse(datetxt2);

    if(accountsdateselected.isAfter(accountsdateparsed)||accountsdateselected.compareTo(accountsdateparsed)==0)
    {


      var dbdata=await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTS);

      List<Map<String,dynamic>>accounts=dbdata;



      for(int i=0;i<accounts.length;i++) {
        Map<String, dynamic> mapval = accounts[i];
        String dateaccount = mapval[DatabaseTables.ACCOUNTS_date];

        String setupid = mapval[DatabaseTables.ACCOUNTS_setupid];

        String amount = mapval[DatabaseTables.ACCOUNTS_amount];

        String accounttype = mapval[DatabaseTables.ACCOUNTS_type].toString();

        DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(dateaccount);

        DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(datetxt1);

        DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(datetxt2);

        var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupid);


        List<Map<String, dynamic>> accsettings1=value;

        Map ab1=accsettings1[0];

        // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]



         int id1 = ab1["keyid"];
         String data1 = ab1["data"];

        var jsondata1 = jsonDecode(data1);



        String Accountname=jsondata1["Accountname"];






        if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.isBefore(accountsdatend)) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
            {
              data.add(amount);
              data.add("");
            }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
            {
              data.add("");
              data.add(amount);

            }






        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }



        }
        else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
            accountsdateparsed.compareTo(accountsdatend) == 0) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }


        }
        else if (accountsdateparsed.isAfter(accountsdatestart) &&
            accountsdateparsed.isBefore(accountsdatend)) {

          data.add(dateaccount);

          data.add(Accountname);

          if(accounttype.compareTo(DataConstants.debit.toString())==0)
          {
            data.add(amount);
            data.add("");
          }
          else if(accounttype.compareTo(DataConstants.credit.toString())==0)
          {
            data.add("");
            data.add(amount);

          }


        }



        }





      setState(() {

        ledgerdata.clear();

        ledgerdata.addAll(data);
      });

      }
    else{


    }




  }


}