import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/Accountsettings.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';
import 'package:save_flutter/domain/Diarysubjects.dart';
import 'package:save_flutter/domain/MyDiary.dart';
import 'package:save_flutter/mainviews/AddAccountSetup.dart';
import 'package:save_flutter/mainviews/AddMyDiary.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';






class MyDiaryDetailspage extends StatefulWidget {
  final String title;
  final String subjectid;

  final String startdate;
  final String enddate;
  final String subject;




  const MyDiaryDetailspage(
      {Key? key, required this.title, required this.subjectid, required this.startdate, required this.enddate, required this.subject})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyDiaryDetailspage(subjectid,startdate,enddate,subject);
}

class _MyDiaryDetailspage extends State<MyDiaryDetailspage> {

  String subjectid="0";

   String startdate;
   String enddate;
  String subject;

   DatabaseHelper dbhelper=new DatabaseHelper();

   List<MydiaryContent>mydiarycontent_all=[];


  _MyDiaryDetailspage(this.subjectid, this.startdate, this.enddate, this. subject);

  @override
  void initState() {
    // TODO: implement initState




getDiaryDetails(subjectid);


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar: AppBar(
        automaticallyImplyLeading: false,

        flexibleSpace: Container(

            color: Color(0xFF096c6c),
            width: double.infinity,
            padding: EdgeInsets.all(10),
            child: new Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(2, 50, 0, 0),
                        alignment: Alignment.center,
                        child: Center(
                            child: new InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Image.asset(
                                  'images/rightarrow.png',
                                  width: 24,
                                  height: 24,
                                  fit: BoxFit.contain,
                                )))),
                    flex: 1,
                  ),
                  Expanded(
                    child: Container(
                        margin: const EdgeInsets.fromLTRB(10, 50, 0, 0),
                        alignment: Alignment.centerLeft,
                        child: Text(
                         "My diary details : "+ subject,
                          style: TextStyle(
                              fontSize: 13, color: Colors.white),
                        )),
                    flex: 5,
                  ),
                ])) ,
        backgroundColor: Color(0xFF096c6c),
        centerTitle: false,

      ),

      body: Stack(

        children: [

        Align(

        alignment: Alignment.topLeft,
        child: Padding(padding: EdgeInsets.fromLTRB(0, 5, 0, 0),

        child: Container(width: double.infinity,
        height: 50,
        child: (TextButton(onPressed: () {

          downloaddiaryPdf();
        },
        child: Text("Download pdf",style: TextStyle(fontSize: 15),),)),) ,))

          ,

          Align(

              alignment: Alignment.topCenter,
              child: Padding(padding: EdgeInsets.fromLTRB(0, 70, 0, 0),

                child: Column(
                    children: [
                      Expanded(
                        child: Container(child:
                        MediaQuery.removePadding(
                            context: context,
                            removeTop: true,
                            child: ListView.builder(
                              physics: BouncingScrollPhysics(),

                              shrinkWrap: true,

                              itemCount: mydiarycontent_all.length,
                              itemBuilder: (context, index) {
                                return  Padding(padding: EdgeInsets.all(10) ,child: Card(
                                    elevation: 7,
                                    child:

                                    Column(children: [
                                      Container(
                                        width:double.infinity,

                                        height: 70,

                                        child: Row(
                                          children: [

                                          Container(width: 230,
                                          child:Padding(padding: EdgeInsets.all(10),

                                              child: Column(children: [Text(
                                                mydiarycontent_all[index].comments,

                                                style:
                                                TextStyle(color: Colors.black, fontSize: 15),
                                              ),Text(mydiarycontent_all[index].date,maxLines: 1,
                                                style:
                                                TextStyle(color: Colors.black, fontSize: 15),
                                              )],)  ))  ,
                                  Padding(padding: EdgeInsets.fromLTRB(70, 0, 0, 0),

                                    child:GestureDetector(onTap: ()async{

                                      Map results = await Navigator.of(context)
                                          .push(new MaterialPageRoute<dynamic>(
                                        builder: (BuildContext context) {
                                          return new AddMyDiarypage(
                                            title: "My diary details", diaryid: mydiarycontent_all[index].id,
                                          );
                                        },
                                      ));

                                      if (results != null &&
                                          results.containsKey('accountadded')) {


                                        int code=results['accountadded'];
                                        if(code==1)
                                        {

                                          //showAccountDetails();
                                        //  showDiarySubjectData();

                                        }
                                      }






                                    },

                                      child:Image.asset("images/edit.png",width: 20,height: 20,) ,)


                                  ),







                                          ],


                                        ),


                                      ),

                                    ],)










                                ));
                              },
                            ))
                        ),flex: 3,
                      ),

                    ]
                ),
              )





          ),
        ],

      ),



    );
  }



  downloaddiaryPdf() async
  {

    var d=DateTime.now().toString();

    Directory tempDir = await getTemporaryDirectory();
    String tempPath = tempDir.path;
    var filePath = tempPath + '/'+d+'.pdf';

    File f= await  new File(filePath).create();
    PdfDocument document = PdfDocument();

    PdfPage page=  document.pages.add();

   page.graphics.drawString(
       subject, PdfStandardFont(PdfFontFamily.helvetica, 20),
        brush: PdfSolidBrush(PdfColor(0, 0, 0)),
        bounds: Rect.fromLTWH(0, 0, 500, 50));

   double m=40;

    for (MydiaryContent md in mydiarycontent_all)
      {

        String a=md.date+"\n"+md.comments;

        page.graphics.drawString(
           a, PdfStandardFont(PdfFontFamily.helvetica, 20),
            brush: PdfSolidBrush(PdfColor(0, 0, 0)),
            bounds: Rect.fromLTWH(0, m, 500, 50));

        m=m+100;

      }


    f.writeAsBytes(document.save());
// Dispose the document.
    document.dispose();

    OpenFile.open(f.path);
  }


  getDiaryDetails(String subjectid) async
  {

    bool isdateexist=false;

    if(startdate.compareTo("Select start date")!=0&&enddate.compareTo("Select end date")!=0)

      {
        isdateexist=true;

      }
    else{

      isdateexist=false;
    }





    List<Map<String,dynamic>> a= await dbhelper.queryAllRows(DatabaseTables.DIARY_table);

    List<MydiaryContent>mydiarycontent=[];

    for (Map ab in a) {

      int id = ab["keyid"];
      String data = ab["data"];
      var jsondata = jsonDecode(data);

      String date = jsondata["date"];
      String contents = jsondata["content"];
      String subject = jsondata["subject"];

      DateTime accountsdateparsed = new DateFormat("dd-MM-yyyy").parse(date);

      if(subject.compareTo(subjectid)==0)
        {

          if(isdateexist) {
            DateTime accountsdatestart = new DateFormat("dd-MM-yyyy").parse(startdate);

            DateTime accountsdatend = new DateFormat("dd-MM-yyyy").parse(enddate);
            if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
                accountsdateparsed.isBefore(accountsdatend)) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
            else if (accountsdateparsed.isAfter(accountsdatestart) &&
                accountsdateparsed.compareTo(accountsdatend) == 0) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
            else if (accountsdateparsed.compareTo(accountsdatestart) == 0 &&
                accountsdateparsed.compareTo(accountsdatend) == 0) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }

            else if (accountsdateparsed.isAfter(accountsdatestart) &&
                accountsdateparsed.isBefore(accountsdatend)) {
              MydiaryContent content = new MydiaryContent(
                  id.toString(), subjectid, "0", date, contents);
              mydiarycontent.add(content);
            }
          }
          else{

            MydiaryContent content = new MydiaryContent(
                id.toString(), subjectid, "0", date, contents);
            mydiarycontent.add(content);
          }

        }



    }

    setState(() {

      mydiarycontent_all.clear();
      mydiarycontent_all.addAll(mydiarycontent);

    });


  }

}