import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';
import 'package:save_flutter/domain/LiabilityData.dart';
import 'package:save_flutter/mainviews/AddInsurance.dart';
import 'package:save_flutter/mainviews/AddLiability.dart';
import 'package:save_flutter/mainviews/CashbankAccountDetails.dart';


import 'package:save_flutter/domain/Paymentdata.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:save_flutter/mainviews/AddRecipt.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

import 'AddBillVoucher.dart';
import 'AddPayment.dart';
import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;


class InsuranceListPage extends StatefulWidget {
  final String title;

  const InsuranceListPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _InsuranceListPage();

}

class _InsuranceListPage extends State<InsuranceListPage> {


  String date = "",
      date1 = "";
  String datetxt1 = "Select start date";
  String datetxt2 = "Select end date";
  List<String>tableheaddata = ["Account name", "Debit", "Credit", "Action"];

  DatabaseHelper dbhelper = new DatabaseHelper();
  List<AccountSetupData> accsetupdata = [];
  List<String> cashbankaccountata = [];

  List<LiabilityData>lbdata=[];

  List<Cashbankaccount> cashbankdata = [];


  @override
  void initState() {
    // TODO: implement initState

    getInsuranceData();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Insurance",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: Stack(

          children:[

            Align(

                alignment: Alignment.topCenter,
                child:   Padding(padding: EdgeInsets.fromLTRB(15, 10, 15, 10),


                  child: Container(child:
                  MediaQuery.removePadding(
                      context: context,
                      removeTop: true,
                      child: ListView.builder(
                        physics: BouncingScrollPhysics(),

                        shrinkWrap: true,

                        itemCount: lbdata.length,
                        itemBuilder: (context, index) {
                          return  Padding(padding: EdgeInsets.all(4) ,child: Card(
                            elevation: 6,
                            child: Container(


                              width: double.infinity,


                              child: Padding(



                                  padding: EdgeInsets.all(10),
                                  child: Column(

                                    children: [

                                      Padding(



                                          padding: EdgeInsets.all(5),
                                          child:
                                          Row(
                                            children: [

                                              Expanded(
                                                child: Text("Account name  "),
                                                flex: 2,
                                              ),
                                              Expanded(
                                                child: Text(":"),
                                                flex: 1,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  lbdata[index].name,
                                                  style: TextStyle(fontSize: 13),
                                                ),
                                                flex: 2,
                                              )


                                            ],

                                          )),

                                      Padding(



                                          padding: EdgeInsets.all(5),
                                          child:
                                          Row(
                                            children: [

                                              Expanded(
                                                child: Text("Amount  "),
                                                flex: 2,
                                              ),
                                              Expanded(
                                                child: Text(":"),
                                                flex: 1,
                                              ),
                                              Expanded(
                                                child: Text(
                                                  lbdata[index].amount,
                                                  style: TextStyle(fontSize: 13),
                                                ),
                                                flex: 2,
                                              )


                                            ],

                                          )),


                                      Divider(
                                        thickness: 1,
                                        color: Colors.black26,
                                      ),

                                      Padding(
                                          padding: EdgeInsets.all(2),
                                          child: Row(
                                            children: [
                                              Expanded(
                                                child: TextButton(
                                                  onPressed: () async {



                                                    Map results = await Navigator.of(context)
                                                        .push(new MaterialPageRoute<dynamic>(
                                                      builder: (BuildContext context) {
                                                        return new AddInsurancePage(
                                                          title: "AddLiability", liabilityid: lbdata[index].id,
                                                        );
                                                      },
                                                    ));

                                                    if (results != null &&
                                                        results.containsKey('accountsetupdata')) {
                                                      setState(() {
                                                        var acc_selected =
                                                        results['accountsetupdata'];

                                                        int acs =
                                                        acc_selected as int;

                                                        if(acs!=0) {



                                                          getInsuranceData();



                                                        }




                                                      });
                                                    }






                                                  },
                                                  child: Text(
                                                    "Edit",
                                                    style: TextStyle(
                                                        color: Colors.lightGreen),
                                                  ),
                                                ),
                                                flex: 2,
                                              ),
                                              Container(
                                                width: 1,
                                                height: 40,
                                                color: Colors.grey,
                                              ),
                                              Expanded(
                                                child: TextButton(
                                                  onPressed: () {

                                                    Widget yesButton = TextButton(
                                                        child: Text("Yes"),
                                                        onPressed: () async {

                                                          Navigator.pop(context);

                                                          deleteLiabilityData( lbdata[index].id,index);


                                                        });



                                                    Widget noButton = TextButton(
                                                      child: Text("No"),
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                      },
                                                    );

                                                    // set up the AlertDialog
                                                    AlertDialog alert = AlertDialog(
                                                      title: Text("Save"),
                                                      content: Text("Do you want to delete now ?"),
                                                      actions: [yesButton, noButton],
                                                    );

                                                    // show the dialog
                                                    showDialog(
                                                      context: context,
                                                      builder: (BuildContext context) {
                                                        return alert;
                                                      },
                                                    );





                                                  },
                                                  child: Text(
                                                    "Delete",
                                                    style: TextStyle(
                                                        color: Colors.redAccent),
                                                  ),
                                                ),
                                                flex: 2,
                                              ),
                                            ],
                                          ))
                                    ],



                                  )),
                            ),
                          ));
                        },
                      ))
                  ),)

            ),



            Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                    padding: EdgeInsets.all(12),
                    child: FloatingActionButton(
                      onPressed: () async {

                        Map results = await Navigator.of(context)
                            .push(new MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) {
                            return new AddInsurancePage(
                              title: "AddInsurance", liabilityid: '0',
                            );
                          },
                        ));

                        if (results != null &&
                            results.containsKey('accountsetupdata')) {
                          setState(() {
                            var acc_selected =
                            results['accountsetupdata'];

                            int acs =
                            acc_selected as int;

                            if(acs!=0) {



                               getInsuranceData();



                            }




                          });
                        }



                      },
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 29,
                      ),
                      backgroundColor: Colors.blue,
                      elevation: 5,
                      splashColor: Colors.grey,
                    ))),

          ]



      ),










    );


  }

  deleteLiabilityData(String id,int index)
  {
    dbhelper.deleteDataByid(id, DatabaseTables.TABLE_INSURANCE);

    setState(() {

      lbdata.removeAt(index);
    });
  }


  getInsuranceData() async
  {

    // assetdata['name']=accountid;
    // assetdata['amount']=amountcontroller.text.toString();
    // assetdata['purchase_date']=datetxt;
    // assetdata['remind_date']=rminddates;
    // assetdata['remarks']=remarkcontroller.text.toString();

    List<LiabilityData>asd=[];




    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_INSURANCE);
    List<LiabilityData> acsdata = [];


    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["name"];
      String amount=jsondata["amount"];
      LiabilityData asdata=new LiabilityData();
      asdata.id=id.toString();
      asdata.name=await getDataById(Accountname);
      asdata.amount=amount;

      acsdata.add(asdata);



    }

    setState(() {
      lbdata.clear();

      lbdata.addAll(acsdata);
    });

  }


  Future<String> getDataById(String id) async
  {
    List<Map<String, dynamic>> a =
    await dbhelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<String> accountsetupdata = [];
    String Accountname = "";

    // accountsetupdata.add(subjectdata);
    for (Map ab in a) {
      print(ab);

      int subid = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);


      if (subid.toString().compareTo(id) == 0) {
        Accountname = jsondata["Accountname"];
      }
    }

    return Accountname;
  }

}