import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/Accountsettings.dart';
import 'package:save_flutter/mainviews/AddAccountSetup.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Accountsettings',
      theme: ThemeData(primarySwatch: Colors.blueGrey),
      home:
          AccountSettinglistpage(title: 'Accountsettings', accountType: 'all'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class AccountSettinglistpage extends StatefulWidget {
  final String title;

  final String accountType;

  const AccountSettinglistpage(
      {Key? key, required this.title, required this.accountType})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AccountSettinglistpage(accountType);
}

class _AccountSettinglistpage extends State<AccountSettinglistpage> {
  String accountType;

  final dbHelper = new DatabaseHelper();

  _AccountSettinglistpage(this.accountType);

  final String dropdown_account = "Select an account";

  List<String> arr = ["Select an account"];
  List<String> arrradion = ["Bank", "Cash"];
  List<Map<String, dynamic>> accountdata = [];

  int bid = 2, cid = 0;

  List<AccountSetupData> accsetupdata = [];

  List<AccountSetupData> accsetupdatadummy = [];
  final List<String> entries = <String>['A', 'B', 'C'];

  @override
  void initState() {
    // TODO: implement initState
    setupAccountData();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Account setup"),
        centerTitle: false,
      ),
      body: Container(
          width: double.infinity,
          height: double.infinity,
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(15, 10, 15, 15),
                    child: new Theme(
                        data: new ThemeData(
                          hintColor: Colors.black26,
                        ),
                        child: TextField(
                          //controller: namecontroller,

                          decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black26, width: 0.5),
                            ),
                            enabledBorder: OutlineInputBorder(
                              borderSide:
                                  BorderSide(color: Colors.black26, width: 0.5),
                            ),
                            hintText: 'Search...',
                          ),

                          onChanged: (text) {
                            // TemRegData.name=text;

                            accsetupdata.clear();

                            List<AccountSetupData> acs = [];

                            if (text.isEmpty) {
                              setState(() {
                                accsetupdata.addAll(accsetupdatadummy);
                              });
                            } else {
                              for (AccountSetupData a in accsetupdatadummy) {
                                if (a.Accountname.toLowerCase()
                                        .contains(text.toLowerCase()) ||
                                    a.Accountname.toUpperCase()
                                        .contains(text.toUpperCase())) {
                                  acs.add(a);
                                }
                              }

                              setState(() {
                                accsetupdata.clear();

                                accsetupdata.addAll(acs);
                              });
                            }
                          },
                        )),
                  )),
              Align(
                  alignment: Alignment.topLeft,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(5, 90, 5, 5),
                    child: ListView.builder(
                        padding: const EdgeInsets.all(8),
                        itemCount: accsetupdata.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            // color: Colors.amber[colorCodes[index]],
                            child: Center(
                                child: Card(
                              elevation: 5,
                              child: Container(
                                color: Colors.white,
                                width: double.infinity,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(6),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text("Account name  "),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: Text(":"),
                                            flex: 1,
                                          ),
                                          Expanded(
                                            child: Text(
                                              accsetupdata[index].Accountname,
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            flex: 2,
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(6),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text("Category"),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: Text(":"),
                                            flex: 1,
                                          ),
                                          Expanded(
                                            child: Text(
                                              accsetupdata[index].Accounttype,
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            flex: 2,
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(6),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text("Opening balance"),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: Text(":"),
                                            flex: 1,
                                          ),
                                          Expanded(
                                            child: Text(
                                              accsetupdata[index]
                                                  .Amount,
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            flex: 2,
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.all(6),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Text("Type"),
                                            flex: 2,
                                          ),
                                          Expanded(
                                            child: Text(":"),
                                            flex: 1,
                                          ),
                                          Expanded(
                                            child: Text(
                                              accsetupdata[index].Type,
                                              style: TextStyle(fontSize: 13),
                                            ),
                                            flex: 2,
                                          )
                                        ],
                                      ),
                                    ),
                                    Divider(
                                      thickness: 1,
                                      color: Colors.black26,
                                    ),
                                    Padding(
                                        padding: EdgeInsets.all(2),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: TextButton(
                                                onPressed: () async {



                                                  Map results = await Navigator.of(context)
                                                      .push(new MaterialPageRoute<dynamic>(
                                                    builder: (BuildContext context) {
                                                      return new AddAccountSettinglistpage(
                                                          title: "Accountsettings",
                                                          accountType: "", accountsetupid: accsetupdata[index].id,);
                                                    },
                                                  ));
                                                  if (results != null &&
                                                      results.containsKey('accountsetupdata')) {
                                                    setState(() {
                                                      var acc_selected =
                                                      results['accountsetupdata'];

                                                      int acs =
                                                      acc_selected as int;

                                                      if(acs!=0) {
                                                        setupAccountData();
                                                      }




                                                    });
                                                  }






                                                },
                                                child: Text(
                                                  "Edit",
                                                  style: TextStyle(
                                                      color: Colors.lightGreen),
                                                ),
                                              ),
                                              flex: 2,
                                            ),
                                            Container(
                                              width: 1,
                                              height: 40,
                                              color: Colors.grey,
                                            ),
                                            Expanded(
                                              child: TextButton(
                                                onPressed: () {
                                                  deleteAccountSetup(
                                                      accsetupdata[index].id,
                                                      index);
                                                },
                                                child: Text(
                                                  "Delete",
                                                  style: TextStyle(
                                                      color: Colors.redAccent),
                                                ),
                                              ),
                                              flex: 2,
                                            ),
                                          ],
                                        ))
                                  ],
                                ),
                              ),
                            )),
                          );
                        }),
                  )),
              Align(
                  alignment: Alignment.bottomRight,
                  child: Padding(
                      padding: EdgeInsets.all(12),
                      child: FloatingActionButton(
                        onPressed: () async {



                          Map results = await Navigator.of(context)
                              .push(new MaterialPageRoute<dynamic>(
                            builder: (BuildContext context) {
                              return new AddAccountSettinglistpage(
                                  title: "Accountsettings",
                                  accountType: "", accountsetupid: '0',);
                            },
                          ));

                          if (results != null &&
                              results.containsKey('accountsetupdata')) {
                            setState(() {
                              var acc_selected =
                              results['accountsetupdata'];

                              int acs =
                              acc_selected as int;

                              if(acs!=0) {
                                setupAccountData();
                              }




                            });
                          }

                        },
                        child: Icon(
                          Icons.add,
                          color: Colors.white,
                          size: 29,
                        ),
                        backgroundColor: Colors.blue,
                        elevation: 5,
                        splashColor: Colors.grey,
                      ))),
            ],
          )),
    );
  }

  void setupAccountData() async {
    List<Map<String, dynamic>> a =
        await dbHelper.queryAllRows(DatabaseTables.TABLE_ACCOUNTSETTINGS);
    // a.sort((Map a, Map b) =>a['Accountname'].toString().compareTo(b['Accountname'].toString()));
    List<AccountSetupData> accountsetupdata = [];
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      String Accountname = jsondata["Accountname"];
      String Accounttype = jsondata["Accounttype"];
      String Amount = jsondata["Amount"];
      String Type = jsondata["Type"];

      AccountSetupData dataaccsetup = new AccountSetupData();
      dataaccsetup.id = id.toString();
      dataaccsetup.Accountname = Accountname;
      dataaccsetup.Accounttype = Accounttype;
      dataaccsetup.Amount = Amount;
      dataaccsetup.Type = Type;

      accountsetupdata.add(dataaccsetup);
    }

    setState(() {
      //  accountdata= a;
      accsetupdata.clear();

      accountsetupdata.sort((a, b) => a.Accountname.compareTo(b.Accountname));

      accsetupdata.addAll(accountsetupdata);

      accsetupdatadummy.clear();

      accsetupdatadummy.addAll(accsetupdata);
    });
  }

  void deleteAccountSetup(String id, int index) {
    Widget yesButton = TextButton(
      child: Text("Yes"),
      onPressed: () {
        dbHelper.deleteDataByid(id, DatabaseTables.TABLE_ACCOUNTSETTINGS).then(
            (value) => {
                  Navigator.pop(context),
                  accsetupdata.removeAt(index),
                  refreshlist()
                });
      },
    );

    Widget noButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );

    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Save"),
      content: Text("Do you want to delete now ?"),
      actions: [yesButton, noButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void refreshlist() {
    List<AccountSetupData> acs = [];

    acs.addAll(accsetupdata);

    accsetupdata.clear();

    setState(() {
      accsetupdatadummy.clear();

      accsetupdata.addAll(acs);

      accsetupdatadummy.addAll(accsetupdata);
    });
  }
}
