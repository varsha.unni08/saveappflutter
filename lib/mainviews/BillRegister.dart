import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/Accountledger.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';


import 'package:save_flutter/domain/Paymentdata.dart';
import 'package:horizontal_data_table/horizontal_data_table.dart';
import 'package:save_flutter/mainviews/AddRecipt.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:syncfusion_flutter_pdf/pdf.dart';
import 'package:open_file/open_file.dart';


import 'package:flutter_picker/flutter_picker.dart';
import 'dart:ui' as ui;








class BillRegisterPage extends StatefulWidget {

  final String title;




  const BillRegisterPage({Key? key, required this.title})
      : super(key: key);

  @override
  State<StatefulWidget> createState()  => _BillPageDetailsPage(title);

}


class _BillPageDetailsPage extends State<BillRegisterPage> {

  String title;




  String accountname="";

  DatabaseHelper dbhelper=new DatabaseHelper();

  List<String>accdata=[];

  List<String>tableheaddata = [

    "Date",
    "Billno",
    "Customer",

    "Amount"
  ];


  _BillPageDetailsPage(this.title);

  @override
  void initState() {
    // TODO: implement initState

checkBillData();
    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
    leading: IconButton(
    icon: Icon(Icons.arrow_back, color: Colors.white),
    onPressed: () => Navigator.of(context).pop(),
    ),
    title: Text("Bill register",style: TextStyle(fontSize: 14),),
    centerTitle: false,
    ),
      body: Stack(
          children: [

            (accdata.length>0)?    Padding(padding: EdgeInsets.fromLTRB(10, 10, 10, 0),




                child: MediaQuery.removePadding(
                  context: context,
                  removeTop: true, child:GridView.builder(
                  physics: BouncingScrollPhysics(),

                  shrinkWrap: true,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 4,
                      crossAxisSpacing: 0.0,
                      mainAxisSpacing: 0.0,
                      childAspectRatio: 2.0

                  ),
                  itemCount: tableheaddata.length,
                  itemBuilder: (context, index) {
                    return  Container(
                        height: 50,
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.black54,
                            width: 0.3,
                          ),
                        ),

                        child:Center(child:Text(tableheaddata[index],style: TextStyle(fontSize: 13,color: Colors.black),maxLines: 2,)));


                  },
                )
                  ,)



            ):Container(),

            (accdata.length>0)?    Padding(padding:EdgeInsets.fromLTRB(10, 55, 10, 0)

                ,child:MediaQuery.removePadding(
                    context: context,
                    removeTop: true,
                    child:GridView.builder(
                      physics: BouncingScrollPhysics(),

                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          crossAxisSpacing: 0.0,
                          mainAxisSpacing: 0.0,
                          childAspectRatio:2.0

                      ),
                      itemCount: accdata.length,
                      itemBuilder: (context, index) {
                        return Container(

                          decoration: BoxDecoration(
                            border: Border.all(
                              color: Colors.black54,
                              width: 0.3,
                            ),
                          ),

                          child:

                          Center(child:Text(accdata[index],style: TextStyle(fontSize: 13,color: Colors.black),)),

                        );
                      },
                    ))):Container(),

          ]

      ),



    );
  }


  void checkBillData() async
  {
    List<Map<String, dynamic>> m=await new DatabaseHelper().getAccounDataByEntryid("0");
    List<Map<String, dynamic>> v=m;

    List<String>accountdata=[];

    for(Map a in v) {
      String vouchertype = a[DatabaseTables.ACCOUNTS_VoucherType].toString();

      int vtype=int.parse(vouchertype);

      if(vtype==DataConstants.billvoucher)
        {
          String date = a[DatabaseTables.ACCOUNTS_date];
          String billno = a[DatabaseTables.ACCOUNTS_billVoucherNumber];
          String setupidno = a[DatabaseTables.ACCOUNTS_setupid];

          var value= await new DatabaseHelper().getDataByid(DatabaseTables.TABLE_ACCOUNTSETTINGS, setupidno);


          List<Map<String, dynamic>> accountsettings=value;

          Map ab=accountsettings[0];

          // [{keyid: 18, data: {"Accountname":"Bank charges","Accounttype":"Expense account","Amount":"0","Type":"Debit"}}]

          int id = ab["keyid"];
          String data = ab["data"];

          var jsondata = jsonDecode(data);

          String Accountname = jsondata["Accountname"];



          String amount = a[DatabaseTables.ACCOUNTS_amount];

          accountdata.add(date);
          accountdata.add(billno);
          accountdata.add(Accountname);
          accountdata.add(amount);


        }


    }

    setState(() {

      accdata.clear();
          accdata.addAll(accountdata);
    });




  }



}