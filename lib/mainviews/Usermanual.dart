import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart' show rootBundle;


class UsermanualPage extends StatefulWidget {
  final String title;

  const UsermanualPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _UsermanualPage();

}

class _UsermanualPage extends State<UsermanualPage> {

  String data="";

  @override
  void initState() {
    // TODO: implement initState

    setupUsermanual();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("How to use",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: SingleChildScrollView(

        child:
        Padding(padding: EdgeInsets.all(15),
        child: Text(data,style: TextStyle(fontSize: 14,color: Colors.black),))

       ,
      ),

    );
  }


  setupUsermanual()async
  {
    String message=await rootBundle.loadString('assets/usermanual.txt');


    setState(() {

      data=message;
    });

  }
  }