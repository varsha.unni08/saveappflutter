import 'dart:convert';

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/TemregData.dart';
import 'package:save_flutter/domain/VisitcardData.dart';
import 'package:save_flutter/domain/country.dart';
import 'dart:io';

import 'package:lite_rolling_switch/lite_rolling_switch.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:save_flutter/mainviews/AddVisitingcard.dart';

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: Visitcardpage(title: 'Visiting card history'),
      debugShowCheckedModeBanner: false,
    );
  }
}


class Visitcardpage extends StatefulWidget{


  Visitcardpage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _VisitcardpageState createState() => _VisitcardpageState();
}


class _VisitcardpageState extends State<Visitcardpage> {
  int _counter = 0;

  int code = 0;

  List<VisitcardData> vcarddata = [];

DatabaseHelper dbhelper=new DatabaseHelper();

  List<String> cardimagebackgrounddata = [
    "cone.jpg",
    "ctwo.jpg",
    "cthree.jpg",
    "cfour.jpg",
    "cfive.jpg",
    "csix.jpg",
    "cseven.jpg",
    "ceight.jpg"
  ];

  @override
  void initState() {
    // TODO: implement initState


showVisitingcards();

    super.initState();


    // sleep(const Duration(seconds:6));
    // startTime();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(

      appBar: AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Visiting card history"),
        centerTitle: false,
      ),

      body: Container(
        width: double.infinity,
        height: double.infinity,

        child: Stack(

          children: [

            Align(
                alignment: Alignment.topLeft,
                child: Padding(
                  padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                  child: ListView.builder(
                      padding: const EdgeInsets.all(8),
                      itemCount: vcarddata.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(


                          // color: Colors.amber[colorCodes[index]],
                          child: InkWell(child: Center(
                              child: Card(
                                elevation: 5,
                                child: Container(
                                  color: Colors.white,
                                  width: double.infinity,
                                  child: Row(children: [

                                    Expanded(child: Image.asset("images/"+cardimagebackgrounddata[int.parse(vcarddata[index].cardbg)]),flex: 1,),

                                    Expanded(child: Column(
                                      children: [
                                        Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Row(
                                            children: [


                                              Text(
                                                vcarddata[index].name,
                                                style: TextStyle(fontSize: 13),
                                              ),


                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Row(
                                            children: [

                                              Text(
                                                vcarddata[index].address,
                                                style: TextStyle(fontSize: 13),
                                              ),

                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Row(
                                            children: [

                                              Text(
                                                vcarddata[index]
                                                    .email,
                                                style: TextStyle(fontSize: 13),

                                              )
                                            ],
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(6),
                                          child: Row(
                                            children: [

                                              Text(
                                                vcarddata[index].phone,
                                                style: TextStyle(fontSize: 13),
                                              ),

                                            ],
                                          ),
                                        ),

                                        Padding(
                                            padding: EdgeInsets.all(2),
                                            child: Row(
                                              children: [

                                                Expanded(
                                                  child: TextButton(
                                                    onPressed: () {

                                                      Widget yesButton = TextButton(
                                                        child: Text("Yes"),
                                                        onPressed: () {



                                                          dbhelper.deleteDataByid(vcarddata[index].id, DatabaseTables.TABLE_VISITCARD).then((value) {


                                                            showVisitingcards();

                                                          });
                                                        },
                                                      );

                                                      Widget noButton = TextButton(
                                                        child: Text("No"),
                                                        onPressed: () {
                                                          Navigator.pop(context);
                                                        },
                                                      );

                                                      // set up the AlertDialog
                                                      AlertDialog alert = AlertDialog(
                                                        title: Text("Save"),
                                                        content: Text("Do you want to delete now ?"),
                                                        actions: [yesButton, noButton],
                                                      );

                                                      // show the dialog
                                                      showDialog(
                                                        context: context,
                                                        builder: (BuildContext context) {
                                                          return alert;
                                                        },
                                                      );








                                                    },
                                                    child: Text(
                                                      "Delete",
                                                      style: TextStyle(
                                                          color: Colors.redAccent),
                                                    ),
                                                  ),
                                                  flex: 2,
                                                ),
                                              ],
                                            ))
                                      ],
                                    ),flex: 2,)

                                  ],),
                                ),
                              )),
                          onTap:() async {

                            Map results = await Navigator.of(context)
                                .push(new MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) {
                                return new AddVisitcardpage(
                                  title: "AddVisitingcard",
                                  cardid: vcarddata[index].id,);
                              },
                            ));

                            if (results != null &&
                                results.containsKey('accountsetupdata')) {
                              setState(() {
                                var acc_selected =
                                results['accountsetupdata'];

                                int acs =
                                acc_selected as int;

                                if(acs!=0) {
                                 showVisitingcards();
                                }




                              });
                            }

                          } ),
                        );
                      }),
                )),

            Align(
                alignment: Alignment.bottomRight,
                child: Padding(
                    padding: EdgeInsets.all(12),
                    child: FloatingActionButton(
                      onPressed: () async {



                        Map results = await Navigator.of(context)
                            .push(new MaterialPageRoute<dynamic>(
                          builder: (BuildContext context) {
                            return new AddVisitcardpage(
                              title: "Add visit card", cardid: '',
                             );
                          },
                        ));

                        if (results != null &&
                            results.containsKey('accountsetupdata')) {
                          setState(() {
                            var acc_selected =
                            results['accountsetupdata'];

                            int acs =
                            acc_selected as int;

                            // if(acs!=0) {
                            //   setupAccountData();
                            // }




                          });
                        }

                      },
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 29,
                      ),
                      backgroundColor: Colors.blue,
                      elevation: 5,
                      splashColor: Colors.grey,
                    ))),
          ],

        ),

      ),
    );
  }


  showVisitingcards()async
  {
    List<Map<String, dynamic>> a= await dbhelper.queryAllRows(DatabaseTables.TABLE_VISITCARD);

    List<VisitcardData> accountsetupdata = [];
    for (Map ab in a) {
      print(ab);

      int id = ab["keyid"];
      String data = ab["data"];

      var jsondata = jsonDecode(data);

      VisitcardData datavisit=new VisitcardData();

      datavisit.id=id.toString();
      datavisit.name=jsondata['name'];
      datavisit.phone=jsondata['phone'];
      datavisit.email=jsondata['email'];
      datavisit.address=jsondata['address'];
      datavisit.whatsapp=jsondata['whatsapp'];
      datavisit.designation=jsondata['designation'];
      datavisit.website=jsondata['website'];
      datavisit.company=jsondata['company'];
      datavisit.cardbg=jsondata['cardbg'].toString();
      datavisit.fb=jsondata['fb'];
      datavisit.youtube=jsondata['youtube'];
      datavisit.instagram=jsondata['instagram'];





      accountsetupdata.add(datavisit);
    }



    setState(() {
      vcarddata.clear();
      vcarddata=accountsetupdata;
    });




  }
}