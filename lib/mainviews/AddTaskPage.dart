

import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/database/DBTables.dart';
import 'package:save_flutter/database/DatabaseHelper.dart';
import 'package:save_flutter/domain/AccountSetupdata.dart';
import 'package:save_flutter/domain/Accountsettings.dart';
import 'package:save_flutter/domain/CashBankAccountDart.dart';
import 'package:save_flutter/mainviews/AddAccountSetup.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'dart:ui' as ui;
import 'package:intl/intl.dart';




class AddTaskListPage extends StatefulWidget {
  final String title;
  final String taskid;






  const AddTaskListPage(
      {Key? key, required this.title, required this.taskid})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _AddTaskListPage(taskid);
}

class _AddTaskListPage extends State<AddTaskListPage> {
String taskid="0";


_AddTaskListPage(this.taskid);

  TextEditingController namecontroller=new TextEditingController();


  DatabaseHelper dbhelper = new DatabaseHelper();

  String datetxt1="Select date";
  String tasktime="Select task time";
  String reminddate="Select remind date";

  List<String> arrstatus=["Initial","Completed","Postponed"];

  String status="Initial";

  bool isStatusvisible=false;

  String taskdate="Select task date";
  String    date="";

  String time="";


  String    rmdate="";



  @override
  void initState() {
    // TODO: implement initState

    if(taskid.compareTo("0")!=0)
      {
        setupDataForEditing( taskid);
      }



    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold( resizeToAvoidBottomInset: true,

      appBar:  AppBar(
        backgroundColor: Color(0xFF096c6c),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () =>  Navigator.pop(context,{"accountsetupdata":1}),
        ),
        title: Text("Add Task",style: TextStyle(fontSize: 14),),
        centerTitle: false,
      ),

      body: Column(

        children: [



          Padding(
            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
            // padding: EdgeInsets.all(15),
            child: new Theme(data: new ThemeData(
              hintColor: Colors.black,

            ), child: TextField(

              controller: namecontroller,


              decoration: InputDecoration(
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 0.5),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black, width: 0.5),
                ),
                hintText: 'Name',



              ),




            )),
          ),

          Container(child:     Padding(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/calendar.png",
                          width: 25,
                          height: 25,
                        )),
                    Container(
                        child: TextButton(
                          onPressed: () async {

var now=DateTime.now();

date= now.day
    .toString() +
    "-" +
    now.month
        .toString() +
    "-" +
    now.year
        .toString();


showModalBottomSheet(
    context: context,
    builder: (context) {
      return Container(
          child: Column(children: [
            Expanded(
              child: CupertinoDatePicker(
                mode: CupertinoDatePickerMode
                    .date,
                initialDateTime: DateTime(
                    now.year,
                    now.month,
                    now.day),
                onDateTimeChanged:
                    (DateTime newDateTime) {



                  date= newDateTime.day
                      .toString() +
                      "-" +
                      newDateTime.month
                          .toString() +
                      "-" +
                      newDateTime.year
                          .toString();


                  // taskdate=date;



                  //print(date);
                  // Do something
                },
              ),
              flex: 2,
            ),
            Padding(
              padding: const EdgeInsets.all(2),
              child: Container(
                height: 50,
                width: 150,
                decoration: BoxDecoration(
                    color: Color(0xF0233048),
                    borderRadius:
                    BorderRadius.circular(
                        10)),
                child: TextButton(
                  onPressed: () {
                    Navigator.pop(context);

                    setState(() {
                      taskdate = date;
                    });
                  },
                  child: Text(
                    'Ok',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 15),
                  ),
                ),
              ),
            )
          ]));
    });




                          },
                          child:SizedBox(width: 210,child: Text(
                              taskdate,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ))
                  ],
                ),
              ))),
          Container(child:     Padding(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/clock.png",
                          width: 25,
                          height: 25,
                        )),
                    Container(
                        child: TextButton(
                          onPressed: () async {

                            TimeOfDay selectedTime = TimeOfDay.now();
                            final TimeOfDay? timeOfDay = await showTimePicker(
                              context: context,
                              initialTime: selectedTime,
                              initialEntryMode: TimePickerEntryMode.dial,

                            );
                            if(timeOfDay != null && timeOfDay != selectedTime)
                            {

                              
                              setState(() {
                                selectedTime = timeOfDay;

                                tasktime=timeOfDay.format(context);
                              });
                            }
                            else{

                              setState(() {


                                tasktime=selectedTime.format(context);
                              });
                            }





                          },
                          child:SizedBox(width: 210,child: Text(
                              tasktime,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ))
                  ],
                ),
              ))),
          Container(child:     Padding(
              padding: EdgeInsets.fromLTRB(15, 10, 15, 10),
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black38)),
                child: Row(
                  textDirection: ui.TextDirection.rtl,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.all(10),
                        child: Image.asset(
                          "images/calendar.png",
                          width: 25,
                          height: 25,
                        )),
                    Container(
                        child: TextButton(
                          onPressed: () async {


                            var now=DateTime.now();

                            rmdate= now.day
                                .toString() +
                                "-" +
                                now.month
                                    .toString() +
                                "-" +
                                now.year
                                    .toString();


                            showModalBottomSheet(
                                context: context,
                                builder: (context) {
                                  return Container(
                                      child: Column(children: [
                                        Expanded(
                                          child: CupertinoDatePicker(
                                            mode: CupertinoDatePickerMode
                                                .date,
                                            initialDateTime: DateTime(
                                                now.year,
                                                now.month,
                                                now.day),
                                            onDateTimeChanged:
                                                (DateTime newDateTime) {



                                              rmdate= newDateTime.day
                                                  .toString() +
                                                  "-" +
                                                  newDateTime.month
                                                      .toString() +
                                                  "-" +
                                                  newDateTime.year
                                                      .toString();


                                              // taskdate=date;



                                              //print(date);
                                              // Do something
                                            },
                                          ),
                                          flex: 2,
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(2),
                                          child: Container(
                                            height: 50,
                                            width: 150,
                                            decoration: BoxDecoration(
                                                color: Color(0xF0233048),
                                                borderRadius:
                                                BorderRadius.circular(
                                                    10)),
                                            child: TextButton(
                                              onPressed: () {
                                                Navigator.pop(context);

                                                setState(() {
                                                  reminddate = rmdate;
                                                });
                                              },
                                              child: Text(
                                                'Ok',
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: 15),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]));
                                });





                          },
                          child:SizedBox(width: 210,child: Text(
                              reminddate,
                              style: TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          ),
                        ))
                  ],
                ),
              ))),



          (isStatusvisible)?  Padding(
              padding: const EdgeInsets.all(8),
              child:Container(
                width: double.infinity,
                height: 60.0,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: Colors.black,
                    // red as border color
                  ),
                ),
                child: DropdownButtonHideUnderline(
                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: InputDecorator(
                      decoration: const InputDecoration(border: OutlineInputBorder()),
                      child: DropdownButtonHideUnderline(
                        child: DropdownButton(

                          value: status,
                          items: arrstatus
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                          onChanged: (String? newValue) {
                            setState(() {
                              status = newValue!;

                              //TemRegData.language=languagedropdown;
                            });
                          },
                          style: Theme.of(context).textTheme.bodyText1,

                        ),
                      ),
                    ),
                  ),
                ),
              )):Container(),












          Padding(
            padding: EdgeInsets.fromLTRB(15, 20, 15, 15),

            child: Container(

              width: double.infinity,
              height: 55,
              decoration: BoxDecoration(

                  color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
              child:Align(
                alignment: Alignment.center,
                child: TextButton(

                  onPressed:() {


                    // JSONObject jsonObject = new JSONObject();
                    // jsonObject.put("name", edtName.getText().toString());
                    // jsonObject.put("date", date);
                    // jsonObject.put("time", timeselected);
                    // jsonObject.put("status", 0);
                    // jsonObject.put("reminddate",reminddate);

                    if(namecontroller.text.isNotEmpty)
                      {
                        if(taskdate.isNotEmpty)
                        {
                          if(reminddate.isNotEmpty)
                          {
                            var m=new Map();
                            m['name']=namecontroller.text.toString();
                            m['date']=taskdate;

                            if(tasktime.compareTo("Select task time")!=0) {
                              m['time'] = tasktime;
                            }
                            else{
                              m['time'] = "";

                            }
                            m['status']=status;
                            m['reminddate']=rmdate;

                            var js=json.encode(m);

                            Map<String, dynamic> data_To_Table=new Map();
                            data_To_Table['data']=js.toString();

                            if(taskid.compareTo("0")==0)
                              {
                                dbhelper.insert(data_To_Table, DatabaseTables.TABLE_TASK);
                              }
                            else{

                              dbhelper.update(data_To_Table, DatabaseTables.TABLE_TASK,taskid);
                            }



                            Navigator.pop(context,{"accountsetupdata":1});

                          }
                          else{

                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text("Select remind date"),
                            ));
                          }

                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select task date"),
                          ));
                        }

                      }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Enter name"),
                      ));
                    }








                  },

                  child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
              ),



              //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
            ),


            // ,
          ),
        ],



      ),
    );
  }





setupDataForEditing(String taskid) async
{

  var v = await dbhelper.getDataByid(
      DatabaseTables.TABLE_TASK, taskid.toString());

  List<Map<String, dynamic>> ab = v;

  Map<String, dynamic> mapdata = ab[0];
  String d = mapdata['data'];

  var jsondata = jsonDecode(d);


  String tdate=jsondata['date'];

  String name=jsondata['name'];
  String time=jsondata['time'];
  String sts=jsondata['status'];
  String rddate=jsondata['reminddate'];

  setState(() {

    namecontroller.text=name;
    reminddate=rddate;
    status=sts;
    tasktime=time;
    taskdate=tdate;

  });


}





}