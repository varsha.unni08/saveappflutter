import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/mainviews/Billvoucher.dart';
import 'package:save_flutter/mainviews/Cashbankstatement.dart';
import 'package:save_flutter/mainviews/IncomeExpenditureStatement.dart';
import 'package:save_flutter/mainviews/Ledger.dart';
import 'package:save_flutter/mainviews/Reminds.dart';
import 'package:save_flutter/mainviews/Report.dart';
import 'package:save_flutter/mainviews/Transactions.dart';

import 'mainviews/BillRegister.dart';

class ReportPage extends StatefulWidget {
  final String title;

  const ReportPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ReportPage();

}

class _ReportPage extends State<ReportPage> {
  @override
  Widget build(BuildContext context) {

    return Scaffold(

        body: Container(

          height: double.infinity,
          width: double.infinity,

          // child:Stack(  alignment: Alignment.topLeft,  children: <Widget>[


            child: Container(
                  margin: const EdgeInsets.fromLTRB(0, 5, 0, 5),

                  child: SingleChildScrollView(
                      child:Column(

                          children: [

                            Card(

                              child:

                              InkWell(

                          child: Container(
                              width: double.infinity,
                              height: 50,

                              child:Padding(

                                padding: EdgeInsets.all(10),

                                child: Align(

                                  alignment: Alignment.centerLeft,
                                  child:TextButton(onPressed: () {

                                    Navigator.push(
                                        context, MaterialPageRoute(builder: (_) =>
                                        TransactionPage(title: "Transaction")));

                                  },

                                    child: Text("Transactions",style: TextStyle(fontSize: 12,color: Colors.black)),


                                  )




                                  ,),


                              )


                          ),
                                onTap:() {

                                  Navigator.push(
                                      context, MaterialPageRoute(builder: (_) =>
                                      TransactionPage(title: "Transaction")));
                                },
                      )

                             ,
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:

                                  InkWell(

                                      onTap:() {

                                        Navigator.push(
                                            context, MaterialPageRoute(builder: (_) =>
                                            LedgerPage(title: "Ledger")));
                                      },

                                child:  Padding(

                                    padding: EdgeInsets.all(10),
                                    child: Align(

                                      alignment: Alignment.centerLeft,
                                      child:TextButton(onPressed: () {  Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          LedgerPage(title: "Ledger")));  },
                                      child:Text("Ledgers",style: TextStyle(fontSize: 12,color: Colors.black)) ,),),
                                  )

                                  )


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,


                                  child:InkWell(

                                    onTap:() {

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          CashbankstatementPage(title: "Cash bank statement")));
                                    },


                                    child:Padding(

                                    padding: EdgeInsets.all(10),
                                    child:
                                    Align(

                                      alignment: Alignment.centerLeft,

                                      child:
                                      TextButton(
                                        onPressed: () {

                                          Navigator.push(
                                              context, MaterialPageRoute(builder: (_) =>
                                              CashbankstatementPage(title: "Cash bank statement")));
                                        },
                                        child:  Text("Cash and Bank Statement",style: TextStyle(fontSize: 12,color: Colors.black)),

                                      )

                                     ,),
                                  ),



                              ),



                              )
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    padding: EdgeInsets.all(10),
                                    child:  InkWell(

                                      onTap:() {

                                        Navigator.push(
                                            context, MaterialPageRoute(builder: (_) =>
                                            IncomeExpenditurePage(title: "IncomeExpenditure statement")));
                                      },

                                  child:  Align(

                                      alignment: Alignment.centerLeft,

                                      child:

                                      Text("Income and Expenditure Statement",style: TextStyle(fontSize: 12,color: Colors.black)),),
                                  ))


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:InkWell(

                                    onTap: (){

                                      Navigator.push(
                                          context, MaterialPageRoute(builder: (_) =>
                                          RemindsListPage(title: "Reminders")));
                                    },

                                      child:Padding(

                                        padding: EdgeInsets.all(10),
                                        child:
                                        Align(

                                          alignment: Alignment.centerLeft,

                                          child:

                                          Text("Reminders",style: TextStyle(fontSize: 12,color: Colors.black)),),
                                      )
                                  )




                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    child:InkWell(
                                        child:
                                        Align(

                                          alignment: Alignment.centerLeft,

                                          child:

                                          Text("List of my Assets",style: TextStyle(fontSize: 12,color: Colors.black)),),

                                            onTap: (){

                                              Navigator.push(
                                                  context, MaterialPageRoute(builder: (_) =>
                                                  ReportPageAccount(title: "Report", accounttype: 'Asset account',)));

                                            },

                                    ),

                                    padding: EdgeInsets.all(10),

                                  )


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    child:InkWell(
                                      child:
                                      Align(

                                        alignment: Alignment.centerLeft,

                                        child:

                                        Text("List of my liabilities",style: TextStyle(fontSize: 12,color: Colors.black)),),

                                      onTap: (){
                                        Navigator.push(
                                            context, MaterialPageRoute(builder: (_) =>
                                            ReportPageAccount(title: "Report", accounttype: 'Liability account',)));

                                      },
                                    ),

                                    padding: EdgeInsets.all(10),

                                  )


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    padding: EdgeInsets.all(10),

                                    child:InkWell(
                                        child:
                                        Align(

                                          alignment: Alignment.centerLeft,

                                          child:

                                          Text("List of my insurance",style: TextStyle(fontSize: 12,color: Colors.black)),),
                                      onTap: (){
                                        Navigator.push(
                                            context, MaterialPageRoute(builder: (_) =>
                                            ReportPageAccount(title: "Report", accounttype: 'Insurance',)));
                                      },


                                    )
                                   ,
                                  )


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    padding: EdgeInsets.all(10),

                                    child:InkWell(

                                      onTap: (){
                                        Navigator.push(
                                            context, MaterialPageRoute(builder: (_) =>
                                            ReportPageAccount(title: "Report", accounttype: 'Investment',)));
                                      },

                                        child:
                                        Align(

                                          alignment: Alignment.centerLeft,

                                          child:

                                          Text("List of my investments",style: TextStyle(fontSize: 12,color: Colors.black)),)

                                    )
                                    ,
                                  )


                              ),
                            ),

                            Card(

                              child: Container(
                                  width: double.infinity,
                                  height: 50,

                                  child:Padding(

                                    padding: EdgeInsets.all(10),
                                    child:
                                        InkWell(

                                          child: Align(

                                            alignment: Alignment.centerLeft,

                                            child:

                                            Text("Bill register",style: TextStyle(fontSize: 12,color: Colors.black)),),

                                          onTap: (){


                                            Navigator.push(
                                                context, MaterialPageRoute(builder: (_) =>
                                                BillRegisterPage(title: "Bill register",)));

                                          },


                                        )




                                    ,
                                  )


                              ),
                            )




                          ])



                  ),




                )











         // ]),




        )




    );

  }

}
