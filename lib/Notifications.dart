import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:intl/intl.dart';
import 'package:save_flutter/domain/MessageData.dart';
import 'package:save_flutter/domain/NotificationMessage.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'mainviews/NotificationDetails.dart';

class NotificationPage extends StatefulWidget {
  final String title;

  const NotificationPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationPage();

}

class _NotificationPage extends State<NotificationPage> {


  List<MessageData>messagedata=[];

  @override
  void initState() {
    // TODO: implement initState

    getNotifications();
    super.initState();

  }



  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Notifications",style: TextStyle(fontSize: 14),),
          centerTitle: false,
        ),

        body: Stack(

          children: [

        Align(
    alignment: Alignment.topLeft,
    child: Padding(padding: EdgeInsets.all(5),

     child: ListView.builder(
        padding: const EdgeInsets.all(8),
            itemCount: messagedata.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                // color: Colors.amber[colorCodes[index]],
                child: Center(
                    child:InkWell(

                      onTap: ()async{

                        print("tapped");

                        Navigator.pushReplacement(context, MaterialPageRoute(
                            builder: (context) => NotificationDetailsPage(title: "Notification details",messagedata: messagedata[index],)
                        )
                        );



                      },

                      child:Card(
                        elevation: 5,
                        child: Container(
                          color: Colors.white,
                          width: double.infinity,


                            
                            // Image.asset("images/ic_launcher.png",width: 50,height: 50,),

                          child:
                          Padding(padding: EdgeInsets.all(8),
                            child: Column(
                              children: [
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].message.title,
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].message.message,
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Row(
                                    children: [

                                      Expanded(
                                        child: Text(
                                          messagedata[index].date
                                          ,
                                          style: TextStyle(fontSize: 13),
                                        ),
                                        flex: 2,
                                      )
                                    ],
                                  ),
                                ),



                              ],
                            ),

                          )



                          ,)
                          ,
                        ),
                      ) ,
                    )




                ,
              );
            }),
    )
        )



          ],


        )



    );

  }

  getNotifications() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
        Uri.parse(DataConstants.baseurl +
            DataConstants.getNotifications +
            "?timestamp=" +
            date.toString()),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'Authorization': datacount.getString(DataConstants.userkey)!
        },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

     var json = jsonDecode(response);;

    NotificationMessage notificationMessage=NotificationMessage.fromJson(json);

    if(notificationMessage.status==1)
      {



for(MessageData m in notificationMessage.data)
  {

    String createddate=m.message.created_date;

    if(createddate!=null)
      {

        DateTime trialdateparsed = new DateFormat("yyyy-MM-dd hh:mm:ss").parse(createddate);

      String   date1=trialdateparsed.day.toString()+"-"+trialdateparsed.month.toString()+"-"+trialdateparsed.year.toString();

      m.date=date1;

      }


  }

setState(() {

  messagedata.clear();
  messagedata.addAll(notificationMessage.data);
});


      }
    else{




    }




  }

}
