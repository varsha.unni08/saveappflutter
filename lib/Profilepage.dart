import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:save_flutter/domain/InvoiceData.dart';
import 'package:save_flutter/mainviews/InvoicePage.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'connection/DataConnection.dart';
import 'domain/CountryData.dart';
import 'domain/CountryList.dart';
import 'domain/Profiledata.dart';
import 'domain/StateData.dart';
import 'domain/Statelist.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

import 'package:http/http.dart' as http;

import 'domain/TemregData.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

void main() {
  // runApp(MyApp());

  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'login',
      theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blueGrey),
      home: ProfilePage(title: 'login'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class ProfilePage extends StatefulWidget {
  final String title;

  const ProfilePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {


  String dropdownValue = 'Select country';
  String dropdownstate="Select state";
  String profileimage="";


  String languagedropdown='Select your language';

  bool  isstateVisible=false,isSponserVisible=false;

  List<CountryData> countrydatalist=[];

  List<Statelist> statedatalist=[];

  List<String> statedata_data=[];

  List<String> countr_data=[];

  late CountryData cddata;

  bool istermsconditionchecked=false;

  String countrid="0",stateid="0";

  String name="";
  String email="";
  String amounttopay="";

  double cgst=0,igst=0,sgst=0,othertaxamount=0,totalamount=0;

  TextEditingController emailcontroller=new TextEditingController();

  TextEditingController namecontroller=new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState

    DataConnectionStatus.check().then((value)
    {

      if (value != null && value) {
        // Internet Present Case

        getProfile();

       // getCountryData();


      }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Check your internet connection"),
        ));

      }
    }
    );
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF096c6c),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Profile"),
          centerTitle: false,
        ),
        body: Stack(children: <Widget>[
          new Container(
            decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new AssetImage("images/splashbg.png"),
                    fit: BoxFit.fill)),
          ),
          SingleChildScrollView(
              child: Column(children: <Widget>[
            Align(
                alignment: Alignment.center,
                child: Container(
                    height: 130.0,
                    width: 130.0,
                    child: Padding(
                        padding: EdgeInsets.all(10),
                        child: Stack(children: [
                          Container(
                            height: 130.0,
                            width: 130.0,

                            child:Image.network(
                                DataConstants.profileimgbaseurl+profileimage,
                               errorBuilder: (context, url, error) => new Icon(Icons.account_circle_rounded,size: 100,),
                            )
                            
                            // child:Image.asset("images/user.png")
                          ),
                          Align(
                              alignment: Alignment.bottomRight,
                              child: FloatingActionButton(
                                onPressed: () async {

                                  final ImagePicker _picker = ImagePicker();
                                  // Pick an image
                                  final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

                               // print(image!.path) ;

                                  if(image!=null) {
                                    File? croppedFile = await ImageCropper
                                        .cropImage(
                                        sourcePath: image.path,
                                        aspectRatioPresets: [
                                          CropAspectRatioPreset.square,
                                          CropAspectRatioPreset.ratio3x2,
                                          CropAspectRatioPreset.original,
                                          CropAspectRatioPreset.ratio4x3,
                                          CropAspectRatioPreset.ratio16x9
                                        ],
                                        androidUiSettings: AndroidUiSettings(
                                            toolbarTitle: 'Cropper',
                                            toolbarColor: Colors.deepOrange,
                                            toolbarWidgetColor: Colors.white,
                                            initAspectRatio: CropAspectRatioPreset
                                                .original,
                                            lockAspectRatio: false),
                                        iosUiSettings: IOSUiSettings(
                                          minimumAspectRatio: 1.0,
                                        )
                                    );


                                    uploadImage(croppedFile);
                                  }



                                },
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                  size: 29,
                                ),
                                backgroundColor: Color(0xffbfefcc),
                                tooltip: 'Capture Picture',
                                elevation: 5,
                                splashColor: Colors.grey,
                              ))
                        ])))),

                Padding(
                  padding: const EdgeInsets.all(15),

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: Row(
                    textDirection: TextDirection.ltr,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment:
                    MainAxisAlignment.center,
                    //Center Row contents horizontally,
                    crossAxisAlignment:
                    CrossAxisAlignment.center,
                    children: <Widget>[
                      Expanded(child:
                      Text("Mobile number :  " ,style: TextStyle(color: Colors.white,fontSize: 14),

                      ),flex:1),
                      Expanded(child: Text("  "+TemRegData.mobilenumber,style: TextStyle(color: Colors.white,fontSize: 14))
                      ,flex: 1,)
                    ],
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.all(15),
                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.white
                  ), child: TextField(

                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      hintText: 'Name',

                    ),
                    onChanged: (text) {
                      TemRegData.name=text;

                    },
                    controller: namecontroller,
                  )),
                ),

                // Padding(
                //     padding: const EdgeInsets.all(15),
                //     child:Container(
                //       width: double.infinity,
                //       height: 60.0,
                //       decoration: BoxDecoration(
                //         border: Border.all(
                //           color: Colors.white,
                //           // red as border color
                //         ),
                //       ),
                //       child: DropdownButtonHideUnderline(
                //         child: ButtonTheme(
                //           alignedDropdown: true,
                //           child: InputDecorator(
                //             decoration: const InputDecoration(border: OutlineInputBorder()),
                //             child: DropdownButtonHideUnderline(
                //               child: DropdownButton(
                //
                //                 value: dropdownValue,
                //                 items: countr_data
                //                     .map<DropdownMenuItem<String>>((String value) {
                //                   return DropdownMenuItem<String>(
                //                     value: value,
                //                     child: Text(value),
                //                   );
                //                 }).toList(),
                //                 onChanged: (String? newValue) {
                //                   setState(() {
                //                     dropdownValue = newValue!;
                //
                //                     DataConnectionStatus.check().then((value)
                //                     {
                //
                //                       if (value != null && value) {
                //                         // Internet Present Case
                //
                //                         getState(dropdownValue);
                //
                //
                //                       }
                //                       else{
                //
                //                         ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //                           content: Text("Check your internet connection"),
                //                         ));
                //
                //                       }
                //                     }
                //                    );
                //
                //
                //
                //                   });
                //
                //                 },
                //                 //  value: dropdownValue,
                //                 style: Theme.of(context).textTheme.bodyText1,
                //
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )),
                //
                //
                //
                // isstateVisible?  Padding(
                //     padding: const EdgeInsets.all(15),
                //     child:Container(
                //       width: double.infinity,
                //       height: 60.0,
                //       decoration: BoxDecoration(
                //         border: Border.all(
                //           color: Colors.white,
                //           // red as border color
                //         ),
                //       ),
                //       child: DropdownButtonHideUnderline(
                //
                //         child: ButtonTheme(
                //           alignedDropdown: true,
                //           child: InputDecorator(
                //             decoration: const InputDecoration(border: OutlineInputBorder()),
                //             child: DropdownButtonHideUnderline(
                //               child: DropdownButton(
                //
                //                 isExpanded: true,
                //                 value: dropdownstate,
                //                 items: statedata_data
                //                     .map<DropdownMenuItem<String>>((String value) {
                //                   return DropdownMenuItem<String>(
                //                     value: value,
                //                     child: Text(value),
                //                   );
                //                 }).toList(),
                //                 onChanged: (String? newValue) {
                //                   setState(() {
                //                     dropdownstate = newValue!;
                //
                //                     checkState(dropdownstate);
                //
                //
                //                   });
                //                 },
                //                 style: Theme.of(context).textTheme.bodyText1,
                //
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     )):new Container(),























                Padding(
                  padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                  // padding: EdgeInsets.all(15),
                  child: new Theme(data: new ThemeData(
                      hintColor: Colors.white
                  ), child: TextField(
                    controller: emailcontroller,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.white, width: 0.5),
                      ),
                      hintText: 'Email',
                    ),

                    onChanged: (text) {
                      TemRegData.email=text;

                    },



                  )),
                ),

                Padding(
                    padding: const EdgeInsets.all(15),
                    child:Container(
                      width: double.infinity,
                      height: 60.0,
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.white,
                          // red as border color
                        ),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: InputDecorator(
                            decoration: const InputDecoration(border: OutlineInputBorder()),
                            child: DropdownButtonHideUnderline(
                              child: DropdownButton(

                                value: languagedropdown,
                                items: DataConstants.arrofLanguages
                                    .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    languagedropdown = newValue!;

                                    TemRegData.language=languagedropdown;
                                  });
                                },
                                style: Theme.of(context).textTheme.bodyText1,

                              ),
                            ),
                          ),
                        ),
                      ),
                    )),


                Padding(
                  padding: const EdgeInsets.all(15),
                  child :   Container(
                    height: 50,
                    width: 150,
                    decoration: BoxDecoration(
                        color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () {
                        // Navigator.push(
                        //     context, MaterialPageRoute(builder: (_) => Otppage(title: "Otp",)));

                        // TemRegData trmg=new TemRegData(namecontroller.value.text,emailcontroller.value.text
                        // ,dropdownValue,dropdownstate,mobilecontroller.value.text,sponsermobilecontroller.value.text,passwordcontroller.value.text,languagedropdown);


                        updateProfile(emailcontroller.value.text,countrid,stateid,namecontroller.value.text);

                                             },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ),
                  ),),


                Padding(
                  padding: const EdgeInsets.all(10),
                  child :   Container(
                    height: 50,
                    width: 150,
                    decoration: BoxDecoration(
                        color: Color(0xF0233048), borderRadius: BorderRadius.circular(10)),
                    child: TextButton(
                      onPressed: () {


                        getBill();






                      },
                      child: Text(
                        'Invoice',
                        style: TextStyle(color: Colors.white, fontSize: 15),
                      ),
                    ),
                  ),),

          ]))
        ]));
  }

  getBill() async
  {
    final datacount = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.getbill+"?timestamp="+date),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!

      },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    print(response);

    InvoiceData invd=InvoiceData.fromJson(jsonDecode(response));

    DateTime dt=DateTime.now();

    String dat=dt.day.toString()+"-"+dt.month.toString()+"-"+dt.year.toString();

    if(invd.status==1)
      {

        String bill=invd.settingsdata.bill_prefix+""+invd.data.bill_no;

        double d=double.parse(invd.data.amt);

        String sgs=invd.data.sgst;
        String cgs=invd.data.cgst;
        String igs=invd.data.igst;

        String other=invd.settingsdata.other_tax;

        if (countrid.compareTo("1")==0) {
          //layout_actualprice.setVisibility(View.GONE);

          if (stateid.compareTo("12")==0) {


            cgst=double.parse(cgs);
            igst=double.parse(igs);
            sgst=double.parse(sgs);
            double othertax=double.parse(other);



            totalamount=d+cgst+sgst;



          }
          else{

            String bill=invd.settingsdata.bill_prefix+""+invd.data.bill_no;

            double d=double.parse(invd.data.amt);



            String other=invd.settingsdata.other_tax;

            cgst=double.parse(cgs);
            igst=double.parse(igs);
            sgst=double.parse(sgs);
            double othertax=double.parse(other);

            totalamount=d+igst;



          }
        }
        else{

          String bill=invd.settingsdata.bill_prefix+""+invd.data.bill_no;

          double d=double.parse(invd.data.amt);





          cgst=double.parse(cgs);
          igst=double.parse(igs);
          sgst=double.parse(sgs);
          double othertax=double.parse(other);





          totalamount=d+othertax;

        }



        Navigator.push(
            context, MaterialPageRoute(builder: (_) => InvoicePage(title: "Payment",date:dat,billno:bill,buyer:name+"\n"+email,transactions: invd.data.cash_transaction_id,amount: d.roundToDouble().toString(),sgst: sgst.roundToDouble().toString(),cgst: cgst.roundToDouble().toString()
            .toString(),igst: igst.roundToDouble().toString(),amounttopay: totalamount.roundToDouble().toString(),countryid: countrid,stateid: stateid,)));

      }




  }



  getCountryData(String countryid,String stat) async
  {

    countrid=countryid;
    stateid=stat;
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(DataConstants.baseurl+DataConstants.getCountry+"?timestamp="+date),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      },

    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    var jsondata=jsonDecode(response);
    print(jsondata);
    CountryList countryList=CountryList.fromJson(jsondata);
    countrydatalist= countryList.data;
    statedata_data.clear();
    List<String>stdata=[];

    String cdata="";

    stdata.add("Select country");
    for(CountryData d in countrydatalist)
    {
      if(d.id==countryid)
        {
          cdata=d.country_name;
        }
      stdata.add(d.country_name);

    }
    setState(() {
      countr_data=stdata;
      dropdownValue=cdata;
      statedata_data=[];


    });

    if(countryid.compareTo("1")==0)
    {
      getState(dropdownValue);
    }
    else{



    }


  }

  getState(String country) async
  {

    String countryid="0";
    for(CountryData d in countrydatalist)
    {
      if(country.compareTo(d.country_name)==0) {
        countryid = d.id;
        countrid=d.id;

        TemRegData.country=country;
        TemRegData.countryid=countrid;

      }

    }



    if(countryid.compareTo("1")==0) {
      ProgressDialog _progressDialog = ProgressDialog();
      _progressDialog.showProgressDialog(
          context, textToBeDisplayed: "Please wait for a moment......");
      var date = new DateTime.now().toIso8601String();
      var dataasync = await http.get(
        Uri.parse(
            DataConstants.baseurl + DataConstants.getState + "?countryid=" +
                countryid + "&timestamp=" + date),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        },

      );
      _progressDialog.dismissProgressDialog(context);
      String response = dataasync.body;
      var jsondata = jsonDecode(response);
      print(jsondata['status']);

      if (jsondata['status'] == 1) {
        try {
          StateData statelist = StateData.fromJson(jsondata);

          statedatalist = statelist.data;

          List<String>stdata = [];
          statedata_data = [];
          stdata.add("Select state");
          for (Statelist st in statelist.data) {

            if(st.id.compareTo(stateid)==0)
              {
                dropdownstate=st.state_name;
              }


            stdata.add(st.state_name);
          }

          setState(() {
            if (countryid.compareTo("1") == 0) {
              isstateVisible = true;
              statedata_data = stdata;
              print(" matched");
            }
            else {
              isstateVisible = false;
              statedata_data = [];

              print("not match");
            }
          });
        } on Exception catch (_) {
          print('never reached');
        }
      } else {
        setState(() {
          isstateVisible = false;
          statedata_data = [];

          print("not match");
        });
      }
    }
    else{

      setState(() {

        isstateVisible=false;
      });
    }

  }


  checkState(String state)
  {

    for (Statelist sd in statedatalist)
    {

      if(sd.state_name.compareTo(state)==0)
      {

        stateid=sd.id;
        TemRegData.state=state;
        TemRegData.stateid=sd.id;
      }

    }
  }


  getProfile() async {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.getUserDetails +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
    );
    String response = dataasync.body;

    print(response);
    var json = jsonDecode(response);

     Profiledata profile=Profiledata.fromJson(json);

     print(profile.data.full_name);

    TemRegData.name=profile.data.full_name;
    TemRegData.email=profile.data.email_id;

    name=profile.data.full_name;
    email=profile.data.email_id;

     namecontroller.text=profile.data.full_name;
     emailcontroller.text=profile.data.email_id;

     setState(() {
       TemRegData.mobilenumber=profile.data.mobile;
       profileimage=profile.data.profile_image;
     });

     countrid=profile.data.country_id;
     stateid=profile.data.state_id;

     getCountryData(profile.data.country_id,profile.data.state_id);

  }


  updateProfile(String email,String countryid,String stateid,String name) async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(DataConstants.baseurl +
          DataConstants.updateProfile +
          "?timestamp=" +
          date.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': datacount.getString(DataConstants.userkey)!
      },
        body: <String, String>{
    "name":name,
   "user_email":email,
    "stateid":stateid,
   "language":"English",
    "country_id":countryid,
   "timestamp":date
        }
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    var json = jsonDecode(response);

    if (json['status'] == 1) {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Profile updated successfully"),
      ));

      Navigator.of(context).pop();
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("failed"),
      ));
    }
  }


  uploadImage(File? file) async {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");


    final datacount = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(DataConstants.baseurl+DataConstants.uploadUserProfile));
    request.headers.addAll( { "Authorization": datacount.getString(DataConstants.userkey)!,
      'Content-Type': 'application/x-www-form-urlencoded'});


    request.files.add(
        http.MultipartFile.fromBytes(
            'file',
            File(file!.path).readAsBytesSync(),
            filename: file.path.split("/").last
        )
    );

    _progressDialog.dismissProgressDialog(context);
    var res = await request.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    getProfile();

    print(responseString);

  }
}
