import 'dart:collection';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:io';

import 'package:save_flutter/HomePage.dart';
import 'package:save_flutter/More.dart';
import 'package:save_flutter/Network.dart';
import 'package:save_flutter/Report.dart';
import 'package:save_flutter/mainviews/Bankvoucher.dart';
import 'package:save_flutter/mainviews/DocumentManager.dart';
import 'package:save_flutter/mainviews/PasswordManager.dart';
import 'package:save_flutter/mainviews/Visitingcard.dart';
import 'package:save_flutter/mainviews/journalvoucher.dart';
import 'package:save_flutter/projectconstants/DataConstants.dart';

import 'Notifications.dart';
import 'Profilepage.dart';
import 'Settings.dart';
import 'connection/DataConnection.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'database/DBTables.dart';
import 'database/DatabaseHelper.dart';
import 'mainviews/AccountSetup.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

 // sleep(const Duration(seconds:3));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'dashboard',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
          primarySwatch: Colors.blueGrey
      ),
      home: MyDashboardPage(title: 'dashboard'),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyDashboardPage extends StatefulWidget {
  MyDashboardPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyDashboardPage createState() => _MyDashboardPage();
}

class _MyDashboardPage extends State<MyDashboardPage> {
  int _counter = 0;
  int _selectedIndex=0;

  bool _fromTop=true;

  final dbHelper = new DatabaseHelper();

  static const List<Widget> _pages = <Widget>[
     HomePage(title: 'home'),
    NetworkPage(title: "Network"),
   ReportPage(title: "report"),

    MorePage(title: "more"),
  ];

  @override
  void initState() {
    // TODO: implement initState

    loadAccountSettingsToDB();
    super.initState();
  }



  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      // _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    //252c45
    return Scaffold(

      appBar: AppBar(
        flexibleSpace:Container(

          width: double.infinity,

          padding: EdgeInsets.all(10),


          child: new Row(

              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [

                Expanded(child: Container(
                    margin: const EdgeInsets.fromLTRB(5, 35, 0, 0),
                    alignment: Alignment.center,
                    child:
                    new InkWell(
                        onTap: () {

                          showGeneralDialog(
                            barrierLabel: "Label",
                            barrierDismissible: true,
                            barrierColor: Colors.black.withOpacity(0.5),
                            transitionDuration: Duration(milliseconds: 700),
                            context: context,
                            pageBuilder: (context, anim1, anim2) {
                              return Align(
                                alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
                                child: Container(
                                  height: 450,
                                  width: 350,
                                  child: SingleChildScrollView(


                                      child: Column(

                                        children: [


                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[

                                                GestureDetector(
                                                onTap: () {

                                                  Navigator.push(
                                                      context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));

                                                },

                                               child: Image.asset(
                                                  'images/journal.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),

                                                ),


                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {

                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => JournalVoucherPage(title: "journal voucher")));


                                                          }, child: Text('Journal voucher',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          )),

                                          Divider(color: Colors.black38,
                                            thickness: 1,),

                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[

                                            GestureDetector(

                                              onTap: () {
                                                Navigator.push(
                                                    context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "bank voucher")));

                                              }

                                              , child: Image.asset(
                                                  'images/bank.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),

                                            ),



                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {


                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => BankVoucherPage(title: "bank voucher")));




                                                          }, child: Text('Bank voucher',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          )),
                                          Divider(color: Colors.black38,
                                            thickness: 1,),
                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[

                                            GestureDetector(
                                              child:  Image.asset(
                                                  'images/accounting.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),
                                              onTap: (){  Navigator.push(
                                                  context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));
                                              },

                                            ),


                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {

                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => AccountSettinglistpage(title: "Payment",accountType: "",)));





                                                          }, child: Text('Account settings',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          )),
                                          Divider(color: Colors.black38,
                                            thickness: 1,),
                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[

                                            GestureDetector(
                                               child: Image.asset(
                                                  'images/card.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),

                                              onTap: (){

                                                Navigator.push(
                                                    context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Visiting card history")));

                                              },

                                            ),


                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {

                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => Visitcardpage(title: "Visiting card history")));



                                                          }, child: Text('Visiting card',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          )),
                                          Divider(color: Colors.black38,
                                            thickness: 1,),
                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[

                                            GestureDetector(
                                               child: Image.asset(
                                                  'images/passcode.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),

                                              onTap: (){

                                                Navigator.push(
                                                    context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "passwordmanager")));

                                              },

                                            ),

                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {

                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => PasswordManagerpage(title: "passwordmanager")));



                                                          }, child: Text('Password manager',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          )),
                                          Divider(color: Colors.black38,
                                            thickness: 1,),
                                          Container(child: Padding(
                                            padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                            //padding: EdgeInsets.symmetric(horizontal: 15),
                                            child: Row(
                                              textDirection: TextDirection.ltr,
                                              children: <Widget>[
                                                GestureDetector(
                                               child: Image.asset(
                                                  'images/foldermanage.png',
                                                  width: 40,
                                                  height: 40,
                                                  fit: BoxFit.cover,
                                                ),
                                                onTap: (){

                                                  Navigator.push(
                                                      context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));




                                                },

                                                ),
                                                Expanded(child: new Theme(data: new ThemeData(
                                                    hintColor: Colors.white
                                                ), child: Align(
                                                    alignment: Alignment.centerLeft,



                                                    child:Padding(


                                                        padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                        child: TextButton(


                                                          onPressed: () {

                                                            Navigator.push(
                                                                context, MaterialPageRoute(builder: (_) => DocumentManagerPage(title: "passwordmanager")));




                                                          }, child: Text('Document manager',style: TextStyle(color: Colors.black,fontSize: 16),),
                                                        )))))
                                              ],
                                            ),
                                          ))
                                          ,
                                          Divider(color: Colors.black38,
                                            thickness: 1,)
                                        ],


                                      )),
                                  margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                              );
                            },
                            transitionBuilder: (context, anim1, anim2, child) {
                              return SlideTransition(
                                position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                                child: child,
                              );
                            },
                          );


                        },
                        child:    Padding(padding: EdgeInsets.all(6),child:ConstrainedBox(constraints: BoxConstraints(minWidth: 15,
                            minHeight:15,
                            maxWidth: 30,
                            maxHeight: 30),


                        child:Image.asset('images/listwhite.png',
                           fit: BoxFit.contain,))))),
                  flex: 1,

                )

                ,

      Expanded(child:  Container(
          margin: const EdgeInsets.fromLTRB(0, 35, 0, 0),
              child:  Padding(padding: EdgeInsets.all(5),child:ConstrainedBox(constraints: BoxConstraints(minWidth: 25,
                    minHeight: 25,
                    maxWidth:40,
                    maxHeight: 40),

             
                   child: Image.asset('images/loginpageiconimg.png',
                    fit: BoxFit.contain,)),
                  ),),flex: 1),

                Expanded(child:  Container(
                    margin: const EdgeInsets.fromLTRB(5, 25, 0, 0),
                    alignment: Alignment.center,
                    child:
                    Text("My Personal App",style: TextStyle(color: Colors.white, fontSize: 12.0, fontWeight: FontWeight.w200),)),flex:
                2,),


                Expanded(child: Container(


                    margin: const EdgeInsets.fromLTRB(6, 25, 0, 0),
                    alignment: Alignment.centerRight,
                    child:

                    new InkWell(
                        onTap: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => ProfilePage(title: "profile",)));



                        }
                        ,

                        child:Padding(padding: EdgeInsets.all(5),child:ConstrainedBox(constraints: BoxConstraints(minWidth: 20,
                            minHeight: 20,
                            maxWidth: 40,
                            maxHeight: 40),child:  Image.asset('images/userprofile.png',
                          fit: BoxFit.contain,))))),
                  flex:1,),


                Expanded(child:   Container(
                    margin: const EdgeInsets.fromLTRB(6, 22, 0, 0),
                    alignment: Alignment.centerRight,
                    child:  new InkWell(
                        onTap: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => NotificationPage(title: "notificationpage",)));



                        }
                        ,

                        child:Padding(padding: EdgeInsets.all(2),child:ConstrainedBox(constraints: BoxConstraints(minWidth: 25,
                            minHeight: 25,
                            maxWidth: 40,
                            maxHeight: 40),child:
                        Image.asset('images/bell.png',
                          fit: BoxFit.contain,))))),
                  flex:1,),

                Expanded(child:  Container(
                    margin: const EdgeInsets.fromLTRB(5, 22, 0, 0),
                    alignment: Alignment.centerRight,
                    child:new InkWell(
                        onTap: () {

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => SettingsPage(title: "Settings",)));



                        }
                        ,

                        child:   Padding(padding: EdgeInsets.all(2),child:ConstrainedBox(constraints: BoxConstraints(minWidth: 25,
                    minHeight: 25,
                    maxWidth: 40,
                    maxHeight: 40),child:
                        Image.asset('images/gear.png',
                           fit: BoxFit.contain,))))),
                  flex:1,)

              ]),














          color: Color(0xFF096c6c),









        ),
        backgroundColor: Color(0xFF096c6c),


        centerTitle: false,

      ),


      body: Container(

          height: double.infinity,
          width: double.infinity,


           child:Stack(  alignment: Alignment.topLeft,  children: <Widget>[


             Align(
              child: Container(
                   margin: const EdgeInsets.fromLTRB(0, 10, 0, 90),

                // padding: const EdgeInsets.all(20),

               child:_pages.elementAt(_selectedIndex),)
             ),

          Align(

            alignment: Alignment.bottomCenter,child: BottomNavigationBar(
              type: BottomNavigationBarType.fixed,
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.grey,
              backgroundColor: Color(0xff096c6c),
              showUnselectedLabels: true,


              onTap:_onItemTap ,

              items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(


            icon: ImageIcon(AssetImage('images/home.png')),
              label: 'Home',

          ),

                BottomNavigationBarItem(
                    icon: ImageIcon(AssetImage('images/network.png')),
                    label: 'Network',
                  backgroundColor: Color(0xff096c6c),


                ),

                BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('images/file.png')),
                  label: 'Report',
                  backgroundColor: Color(0xff096c6c),

                ),

                BottomNavigationBarItem(
                  icon: ImageIcon(AssetImage('images/more.png')),
                  label: 'More',
                  backgroundColor: Color(0xff096c6c),


                ),




              ]

          ),)



          ],







          )


      ),





      //Navigator.of(context).pushReplacementNamed("/home");

    );



  }

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  void loadAccountSettingsToDB() async
  {
    final datacount = await SharedPreferences.getInstance();

    bool? token=datacount.getBool(DataConstants.AccSettingsFirst);

    if(token==null || !token)
      {

        for(String s in DataConstants.expenses )
          {

            var mjobject=new Map();
            mjobject['Accountname']=s;
            mjobject['Accounttype']="Expense account";
            mjobject['Amount']="0";
            mjobject['Type']="Debit";

            var js=json.encode(mjobject);

            Map<String, dynamic> data_To_Table=new Map();
            data_To_Table['data']=js.toString();
            int id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);

            print("expense account : $id");

          }

        for(String s in DataConstants.reciptAccount )
        {



          var mjobject=new Map();
          mjobject['Accountname']=s;
          mjobject['Accounttype']="Income account";
          mjobject['Amount']="0";
          mjobject['Type']="Credit";

          var js=json.encode(mjobject);

          Map<String, dynamic> data_To_Table=new Map();
          data_To_Table['data']=js.toString();
          final id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);

          print("Income account : $id");

        }

        var mjobject=new Map();
        mjobject['Accountname']="Cash";
        mjobject['Accounttype']="Cash";
        mjobject['Amount']="0";
        mjobject['Type']="Debit";

        var js=json.encode(mjobject);

        Map<String, dynamic> data_To_Table=new Map();
        data_To_Table['data']=js.toString();
        final id = await dbHelper.insert(data_To_Table,DatabaseTables.TABLE_ACCOUNTSETTINGS);



        datacount.setBool(DataConstants.AccSettingsFirst,true);
      }


  }
}
